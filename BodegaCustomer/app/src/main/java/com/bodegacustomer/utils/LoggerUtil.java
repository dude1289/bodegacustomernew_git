package com.bodegacustomer.utils;

import android.util.Log;

import com.google.gson.Gson;

/**
 * Created by Abhishek on 25/5/18.
 */

public class LoggerUtil {
    private static final String TAG = "OUTPUT";

    public static void logItem(Object src) {
        Gson gson = new Gson();
        Log.e(TAG, "====:> " + gson.toJson(src));
    }
}
