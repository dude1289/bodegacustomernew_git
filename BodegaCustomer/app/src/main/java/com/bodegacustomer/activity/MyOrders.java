package com.bodegacustomer.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.adapter.AdapterOrders;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.model.request.RequestCancelOrder;
import com.bodegacustomer.model.request.RequestCartItems;
import com.bodegacustomer.model.response.ResponseCancelOrder;
import com.bodegacustomer.model.response.responseOrders.ResponseOrders;
import com.bodegacustomer.retrofit.CancelOrderListener;
import com.bodegacustomer.utils.LoggerUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrders extends BaseActivity implements CancelOrderListener {

    @BindView(R.id.rv_orders)
    RecyclerView rvOrders;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        ButterKnife.bind(this);

        tvTitle.setText("My Orders");
        rvOrders.setLayoutManager(new LinearLayoutManager(context));
        getMyOrders();
    }

    @OnClick({R.id.img_back, R.id.img_cart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_cart:
                goToActivity(context, CartItems.class, null);
                break;
        }
    }

    private void getMyOrders() {
        RequestCartItems items = new RequestCartItems();
        items.setCustomerId(PreferencesManager.getInstance(context).getUserId());

        showLoading();
        LoggerUtil.logItem(items);

        Call<ResponseOrders> call = apiServices.myOrders(items);
        call.enqueue(new Callback<ResponseOrders>() {
            @Override
            public void onResponse(Call<ResponseOrders> call, Response<ResponseOrders> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    AdapterOrders adapter = new AdapterOrders(context, response.body().getLstorder().get(0).getOrderDetails(), MyOrders.this);
                    rvOrders.setVisibility(View.VISIBLE);
                    rvOrders.setAdapter(adapter);
                } else {
                    rvOrders.setVisibility(View.GONE);
                    showMessage("No Order Found");
                }
            }

            @Override
            public void onFailure(Call<ResponseOrders> call, Throwable t) {
                hideLoading();
            }
        });
    }

    @Override
    public void cancelOrder(String productId) {

        RequestCancelOrder order = new RequestCancelOrder();
        order.setCustomerId(PreferencesManager.getInstance(context).getUserId());
        order.setPKOrderDetailsID(productId);

        showLoading();
        LoggerUtil.logItem(order);

        Call<ResponseCancelOrder> call = apiServices.cancelOrder(order);
        call.enqueue(new Callback<ResponseCancelOrder>() {
            @Override
            public void onResponse(Call<ResponseCancelOrder> call, Response<ResponseCancelOrder> response) {
                hideLoading();
                if (response.body().getStatus().equalsIgnoreCase("0"))
                    getMyOrders();
                else showMessage(response.body().getErrorMessage() + "");
            }

            @Override
            public void onFailure(Call<ResponseCancelOrder> call, Throwable t) {
                hideLoading();
            }
        });
    }
}
