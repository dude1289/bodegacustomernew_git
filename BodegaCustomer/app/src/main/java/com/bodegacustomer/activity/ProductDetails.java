package com.bodegacustomer.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bodegacustomer.BuildConfig;
import com.bodegacustomer.R;
import com.bodegacustomer.adapter.AdapterVarients;
import com.bodegacustomer.adapter.SingleProductPageImageSliderPagerRecyclerAdapter;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.model.request.RequestAddToCart;
import com.bodegacustomer.model.request.RequestCartItems;
import com.bodegacustomer.model.request.RequestProductDetails;
import com.bodegacustomer.model.response.ResponseAddToCart;
import com.bodegacustomer.model.response.ResponseCartCount;
import com.bodegacustomer.model.response.responseProductDetails.LstimagesItem;
import com.bodegacustomer.model.response.responseProductDetails.ResponseProductDetails;
import com.bodegacustomer.retrofit.SelectedVarientsListener;
import com.bodegacustomer.utils.LoggerUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
//import com.google.firebase.dynamiclinks.DynamicLink;
//import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
//import com.google.firebase.dynamiclinks.ShortDynamicLink;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bodegacustomer.app.AppConfig.PAYLOAD_BUNDLE;

public class ProductDetails extends BaseActivity implements SelectedVarientsListener {

    @BindView(R.id.tv_product_name)
    TextView tvProductName;
    @BindView(R.id.tv_product_rating)
    TextView tvProductRating;
    @BindView(R.id.tv_review_count)
    TextView tvReviewCount;
    @BindView(R.id.tv_offer_price)
    TextView tvOfferPrice;
    @BindView(R.id.tv_mrp)
    TextView tvMrp;
    @BindView(R.id.tv_seller)
    TextView tvSeller;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.tv_short_desc)
    TextView tvShortDesc;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_delivery)
    TextView tvDelivery;
    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    @BindView(R.id.tv_cart_count)
    TextView tv_cart_count;

    String size = "", color = "", flavour = "", ram = "", Material = "", storage = "";
    String productId = "", producyInfoId = "", vendorId = "";
    String shareUrl = "", productName = "", image = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details);
        ButterKnife.bind(this);
        productId = getIntent().getBundleExtra(PAYLOAD_BUNDLE).getString("productId");
        tvTitle.setText("Products");
        getDetails();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!(PreferencesManager.getInstance(context).getUserId().equalsIgnoreCase("")))
            getCartCount();
    }

    private void getDetails() {
        showLoading();
        RequestProductDetails details = new RequestProductDetails();
        details.setProductID(productId);
        details.setColorID(color);
        details.setFlavorID(flavour);
        details.setLastSelected("");
        details.setMaterialID(Material);
        details.setRamID(ram);
        details.setSizeID(size);
        details.setStorageID(storage);

        if (BuildConfig.DEBUG)
            LoggerUtil.logItem(details);

        Call<ResponseProductDetails> call = apiServices.getQuickView(details);
        call.enqueue(new Callback<ResponseProductDetails>() {
            @Override
            public void onResponse(Call<ResponseProductDetails> call, Response<ResponseProductDetails> response) {
                hideLoading();
                if (BuildConfig.DEBUG)
                    LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    producyInfoId = response.body().getProductInfoCode();
                    vendorId = response.body().getFkVendorId();
                    setData(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponseProductDetails> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void setData(ResponseProductDetails body) {
        tvProductName.setText(body.getProductName());
        tvMrp.setText(String.format("₹ %s", body.getMRP()));
        tvOfferPrice.setText(String.format("₹ %s", body.getOfferedPrice()));
        tvMrp.setPaintFlags(tvMrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvSeller.setText(String.format("%s (Seller)", body.getVendorName()));
        tvDelivery.setText(String.format("Delivery Charges : ₹ %s", body.getDeliveryCharge()));
//        tvShortDesc.setText(body.getShortDescription());
        tvDesc.setText(body.getDescription());
        shareUrl = body.getProductID();
        productName = body.getProductName();
//        image = "http://api.bodegaindia.in" +(body.getImagePath()).replace("..", "");
        List<LstimagesItem> imagesDetailsItems = new ArrayList<>();
        for (int i = 0; i < body.getVarients().size(); i++) {
            if (body.getVarients().get(i).getTitle().equalsIgnoreCase("Images")) {
                imagesDetailsItems = body.getVarients().get(i).getLstimages();
                break;
            }
        }
        LoggerUtil.logItem(imagesDetailsItems);

        SingleProductPageImageSliderPagerRecyclerAdapter imageSliderPagerAdapter = new SingleProductPageImageSliderPagerRecyclerAdapter(this, imagesDetailsItems);

        viewPager.setAdapter(imageSliderPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        int length = tabLayout.getTabCount();
        for (int i = 0; i < length; i++) {
            tabLayout.getTabAt(i).setCustomView(imageSliderPagerAdapter.getTabView(i));
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        AdapterVarients varients = new AdapterVarients(context, body.getVarients(), ProductDetails.this);
        rvCategories.setLayoutManager(new LinearLayoutManager(context));
        rvCategories.setAdapter(varients);
    }

    @OnClick({R.id.btn_add_cart, R.id.btn_buy_now, R.id.img_back, R.id.img_cart, R.id.img_share})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add_cart:
                if (PreferencesManager.getInstance(context).getUserId().equalsIgnoreCase("")) {
                    PreferencesManager.getInstance(context).setProducyInfoId(producyInfoId);
                    PreferencesManager.getInstance(context).setVendorId(vendorId);
                    goToActivityWithFinish(context, Login.class, null);
                } else
                    addToCart(false);
                break;
            case R.id.btn_buy_now:
                if (PreferencesManager.getInstance(context).getUserId().equalsIgnoreCase("")) {
                    PreferencesManager.getInstance(context).setProducyInfoId(producyInfoId);
                    PreferencesManager.getInstance(context).setVendorId(vendorId);
                    goToActivityWithFinish(context, Login.class, null);
                } else
                    addToCart(true);
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_cart:
                if (PreferencesManager.getInstance(context).getUserId().equalsIgnoreCase(""))
                    goToActivityWithFinish(context, Login.class, null);
                else
                    goToActivity(context, CartItems.class, null);
                break;
            case R.id.img_share:
//                createShareLink();

                break;
        }
    }

    @Override
    public void selectedVarients(String size, String color, String flavour, String ram, String Material, String storage) {
        if (size.length() != 0)
            this.size = size;
        if (color.length() != 0)
            this.color = color;
        if (flavour.length() != 0)
            this.flavour = flavour;
        if (ram.length() != 0)
            this.ram = ram;
        if (Material.length() != 0)
            this.Material = Material;
        if (storage.length() != 0)
            this.storage = storage;
        getDetails();
    }

    private void addToCart(boolean moveToCart) {
        RequestAddToCart addToCart = new RequestAddToCart();
        addToCart.setProductInfoCode(producyInfoId);
        addToCart.setCustomerId(PreferencesManager.getInstance(context).getUserId());
        addToCart.setFkVendorId(vendorId);
        addToCart.setProductQuantity("1");
        addToCart.setType("Add");

        showLoading();

        if (BuildConfig.DEBUG)
            LoggerUtil.logItem(addToCart);

        Call<ResponseAddToCart> call = apiServices.addToCart(addToCart);
        call.enqueue(new Callback<ResponseAddToCart>() {
            @Override
            public void onResponse(Call<ResponseAddToCart> call, Response<ResponseAddToCart> response) {
                hideLoading();
                if (BuildConfig.DEBUG)
                    LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    getCartCount();
                    if (moveToCart)
                        goToActivity(context, CartItems.class, null);
                }
                showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponseAddToCart> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void getCartCount() {
        RequestCartItems items = new RequestCartItems();
        items.setCustomerId(PreferencesManager.getInstance(context).getUserId());

        Call<ResponseCartCount> call = apiServices.cartCount(items);
        call.enqueue(new Callback<ResponseCartCount>() {
            @Override
            public void onResponse(Call<ResponseCartCount> call, Response<ResponseCartCount> response) {
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    if (!response.body().getCount().equalsIgnoreCase("0")) {
                        tv_cart_count.setVisibility(View.VISIBLE);
                        tv_cart_count.setText(response.body().getCount());
                    } else tv_cart_count.setVisibility(View.GONE);
                } else tv_cart_count.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseCartCount> call, Throwable t) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        goToActivityWithFinish(context, ContainerActivity.class, null);
        super.onBackPressed();
    }

    //    private Firebase
//    private void createShareLink() {
//        Log.e("main", "create link ");
//        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
//                .setLink(Uri.parse("https://www.blueappsoftware.com/"))
//                .setDomainUriPrefix("referearnpro.page.link")
//                // Open links with this app on Android
//                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
//                // Open links with com.example.ios on iOS
//                //.setIosParameters(new DynamicLink.IosParameters.Builder("com.example.ios").build())
//                .buildDynamicLink();
////click -- link -- google play store -- inistalled/ or not  ----
//        Uri dynamicLinkUri = dynamicLink.getUri();
//        Log.e("main", "  Long refer " + dynamicLink.getUri());
//        //   https://referearnpro.page.link?apn=blueappsoftware.referearnpro&link=https%3A%2F%2Fwww.blueappsoftware.com%2F
//        // apn  ibi link
//        // manuall link
//        String sharelinktext = "https://bodegacustomer.page.link/?" +
//                "link=http://api.bodegaindia.in/productId=" + shareUrl +
//                "&apn=" + getPackageName() +
//                "&st=" + productName +
//                "&sd=" + "Checkout this product on AApkaBiz" +
//                "&si=" + "http://api.bodegaindia.in/images/logo-light.jpg";
//        // shorten the link
//        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
//                //.setLongLink(dynamicLink.getUri())
//                .setLongLink(Uri.parse(sharelinktext))  // manually
//                .buildShortDynamicLink()
//                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
//                    @Override
//                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
//                        if (task.isSuccessful()) {
//                            // Short link created
//                            Uri shortLink = task.getResult().getShortLink();
//                            Uri flowchartLink = task.getResult().getPreviewLink();
//                            Log.e("main ", "short link " + shortLink.toString());
//                            // share app dialog
//                            Intent intent = new Intent();
//                            intent.setAction(Intent.ACTION_SEND);
//                            intent.putExtra(Intent.EXTRA_TEXT, shortLink.toString());
//                            intent.setType("text/plain");
//                            startActivity(intent);
//                        } else {
//                            // Error
//                            // ...
//                            Log.e("main", " error " + task.getException());
//                        }
//                    }
//                });
//    }

}
