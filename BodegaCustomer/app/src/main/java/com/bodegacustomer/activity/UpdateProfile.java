package com.bodegacustomer.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bodegacustomer.R;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.model.request.RequestUpdateProfile;
import com.bodegacustomer.model.response.ResponseUpdateProfile;
import com.bodegacustomer.utils.LoggerUtil;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfile extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.tv_cart_count)
    TextView tvCartCount;
    @BindView(R.id.et_mobile)
    TextInputEditText etMobile;
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.et_name)
    TextInputEditText etName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_info);
        ButterKnife.bind(this);

        imgCart.setVisibility(View.GONE);
        tvCartCount.setVisibility(View.GONE);

        tvTitle.setText("My Profile");

        etEmail.setText(PreferencesManager.getInstance(context).getEmail());
        etName.setText(PreferencesManager.getInstance(context).getFull_Name());
        etMobile.setText(PreferencesManager.getInstance(context).getContact());
    }

    @OnClick({R.id.img_back, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_submit:
                updateProfile();
                break;
        }
    }

    private void updateProfile() {
        showLoading();
        RequestUpdateProfile profile = new RequestUpdateProfile();
        profile.setContact(PreferencesManager.getInstance(context).getContact());
        profile.setEmail(etEmail.getText().toString());
        profile.setName(etName.getText().toString());
        profile.setPkUserId(PreferencesManager.getInstance(context).getUserId());

        LoggerUtil.logItem(profile);
        Call<ResponseUpdateProfile> call = apiServices.UpdateProfile(profile);
        call.enqueue(new Callback<ResponseUpdateProfile>() {
            @Override
            public void onResponse(Call<ResponseUpdateProfile> call, Response<ResponseUpdateProfile> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    PreferencesManager.getInstance(context).setEmail(etEmail.getText().toString());
                    PreferencesManager.getInstance(context).setFull_Name(etName.getText().toString());
                    onBackPressed();
                }
                showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponseUpdateProfile> call, Throwable t) {
                hideLoading();
            }
        });
    }
}
