package com.bodegacustomer.activity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.bodegacustomer.R;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.constants.Cons;
import com.bodegacustomer.model.response.responseMainCategory.ResponseMainCategory;
import com.bodegacustomer.utils.LoggerUtil;
import com.bodegacustomer.utils.NetworkUtils;
import com.github.javiersantos.appupdater.AppUpdater;
import com.github.javiersantos.appupdater.AppUpdaterUtils;
import com.github.javiersantos.appupdater.enums.AppUpdaterError;
import com.github.javiersantos.appupdater.enums.Display;
import com.github.javiersantos.appupdater.enums.UpdateFrom;
import com.github.javiersantos.appupdater.objects.Update;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bodegacustomer.activity.Signup.inviteCode;

public class Splash extends BaseActivity {

    private String productId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        FirebaseApp.initializeApp(context);
//        FirebaseInstanceId.getInstance().getToken();
        if (NetworkUtils.getConnectivityStatus(context) != 0)
            checkUpdate();
        else showMessage(R.string.alert_internet);

//        FirebaseDynamicLinks.getInstance()
//                .getDynamicLink(getIntent())
//                .addOnSuccessListener(this, pendingDynamicLinkData -> {
//                    // Get deep link from result (may be null if no link is found)
//                    Uri deepLink = null;
//                    if (pendingDynamicLinkData != null) {
//                        deepLink = pendingDynamicLinkData.getLink();
//                        LoggerUtil.logItem(deepLink.toString());
//                        String link = deepLink.toString();
//                        if (link.contains("productId")) {
//                            link = link.substring(link.lastIndexOf("=") + 1);
//                            productId = link;
//                            LoggerUtil.logItem(productId);
//                        } else if (link.contains("invitedby")) {
//                            link = link.substring(link.lastIndexOf("=") + 1);
//                            inviteCode = link;
//                            LoggerUtil.logItem(inviteCode);
//                        }
//                    }
//                });
    }

    private AppUpdater updater;

    private void checkUpdate() {
        AppUpdaterUtils appUpdaterUtils = new AppUpdaterUtils(this)
                .setUpdateFrom(UpdateFrom.GOOGLE_PLAY)
                .withListener(new AppUpdaterUtils.UpdateListener() {
                    @Override
                    public void onSuccess(Update update, Boolean isUpdateAvailable) {
                        Log.d("Latest Version", update.getLatestVersion());
                        Log.d("Latest Version Code", "=" + update.getLatestVersionCode());
                        Log.d("Release notes", update.getReleaseNotes());
                        Log.d("URL", "=" + update.getUrlToDownload());
                        Log.d("Is update available?", Boolean.toString(isUpdateAvailable));
                        if (isUpdateAvailable) {
                            if (updater == null) {
                                updater = new AppUpdater(Splash.this).setDisplay(Display.DIALOG);
                                updater.setContentOnUpdateAvailable("Update " + update.getLatestVersion() + " is available to download. Download the latest version to get latest" +
                                        "features, improvements and bug fixes.");
                                updater.setButtonDoNotShowAgain("");
                                updater.setButtonDismiss("");
                                updater.setCancelable(false);
                                updater.start();
                            } else {
                                updater.start();
                            }
                        } else {

//                            TODO
                            if (NetworkUtils.getConnectivityStatus(context) != 0) {
                                getMainCategoryList();
                            } else {
                                createInfoDialog(context, "Alert", getString(R.string.alert_internet));
                            }

                        }
                    }

                    @Override
                    public void onFailed(AppUpdaterError error) {
                        Log.d("AppUpdater Error", "Something went wrong");

                    }
                });
        appUpdaterUtils.start();
    }

    void getMainCategoryList() {
        showLoading();
        Call<ResponseMainCategory> call = apiServices.getMainCategory();
        call.enqueue(new Callback<ResponseMainCategory>() {
            @Override
            public void onResponse(Call<ResponseMainCategory> call, Response<ResponseMainCategory> response) {
                hideLoading();
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    Cons.MainCategoryList_ary_list.clear();
                    Cons.MainCategoryList_ary_list = response.body().getLstmenu().get(0).getMainMenu();
                }
                new Handler().postDelayed(() -> {
                    if (!productId.equalsIgnoreCase("")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("productId", productId);
                        ((BaseActivity) context).goToActivityWithFinish((Activity) context, ProductDetails.class, bundle);
                    } else if (PreferencesManager.getInstance(context).getIsFirstIntro())
                        goToActivityWithFinish(Splash.this, IntroActivity.class, null);
                    else
                        goToActivityWithFinish(context, ContainerActivity.class, null);
                }, 1000);
            }

            @Override
            public void onFailure(Call<ResponseMainCategory> call, Throwable t) {
                hideLoading();
            }
        });

    }
}
