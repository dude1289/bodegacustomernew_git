package com.bodegacustomer.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bodegacustomer.R;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.constants.Cons;
import com.bodegacustomer.fragment.ChangePassword;
import com.bodegacustomer.fragment.ContactUs;
import com.bodegacustomer.fragment.Dashboard;
import com.bodegacustomer.fragment.ProductCategory;
import com.bodegacustomer.model.response.responseMainCategory.ResponseMainCategory;
import com.bodegacustomer.utils.NetworkUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContainerActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    @SuppressLint("StaticFieldLeak")
    public static TextView cart_count;
    boolean doubleBackToExitPressedOnce = false;
    View child = null;
    private TextView logout;
    private LinearLayout dynamic_lo;
    private DrawerLayout shopping_drawer;

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            shopping_drawer.closeDrawers();
            String idxStr = (String) view.getTag();
            try {
                Bundle bundle = new Bundle();
                bundle.putString("CategoryId", Cons.MainCategoryList_ary_list.get(Integer.parseInt(idxStr)).getFKMainCategory());
                bundle.putString("CategoryName", Cons.MainCategoryList_ary_list.get(Integer.parseInt(idxStr)).getMenu());
                Fragment fragment = new ProductCategory();
                fragment.setArguments(bundle);
                ReplaceFragement(fragment, Cons.MainCategoryList_ary_list.get(Integer.parseInt(idxStr)).getMenu());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        try {
            Toolbar containertoolbar = findViewById(R.id.containertoolbar);
            setSupportActionBar(containertoolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setuptoolbar("Dashboard");
            cart_count = findViewById(R.id.cart_count);
            shopping_drawer = findViewById(R.id.drawer_layout);
            shopping_drawer.setScrimColor(getResources().getColor(android.R.color.transparent));
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, shopping_drawer, containertoolbar, R.string.app_name, R.string.app_name);
            shopping_drawer.addDrawerListener(toggle);
            toggle.syncState();
            NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            View hView = navigationView.getHeaderView(0);
            /*..............................................*/
            TextView shoppingorders = hView.findViewById(R.id.shoppingorders);
            TextView change_password = hView.findViewById(R.id.change_password);
            TextView dashboard = hView.findViewById(R.id.dashboard);
            TextView shoppingcontacts = hView.findViewById(R.id.shoppingcontacts);
            TextView share_app = hView.findViewById(R.id.share_app);

            TextView ac_info = hView.findViewById(R.id.ac_info);
            TextView address_book = hView.findViewById(R.id.address_book);
            TextView coupons = hView.findViewById(R.id.coupons);

            logout = hView.findViewById(R.id.logout);

            dynamic_lo = hView.findViewById(R.id.dynamic_lo);

            TextView tv_user_name = hView.findViewById(R.id.tv_user_name);
            tv_user_name.setText("Welcome, " + PreferencesManager.getInstance(context).getFull_Name());

            shoppingorders.setOnClickListener(this);
            shoppingcontacts.setOnClickListener(this);
            change_password.setOnClickListener(this);
            dashboard.setOnClickListener(this);
            share_app.setOnClickListener(this);

            ac_info.setOnClickListener(this);
            address_book.setOnClickListener(this);
            coupons.setOnClickListener(this);

            logout.setOnClickListener(this);

            if (PreferencesManager.getInstance(context).getUserId().equalsIgnoreCase("")) {
                logout.setText("Login");
                change_password.setVisibility(View.GONE);
                shoppingorders.setVisibility(View.GONE);
            } else {
                logout.setText("Logout");
                change_password.setVisibility(View.VISIBLE);
                shoppingorders.setVisibility(View.VISIBLE);
            }

            /*.....................................*/

//            if (checkNetworkConnection()) {
//                getSubCategoryList();
//            } else {
//                createInfoDialog(context, "Alert", getString(R.string.alert_internet));
//            }
//
            ReplaceFragement(new Dashboard(), "Dashboard");


        } catch (Error | Exception e) {
            e.printStackTrace();
        }

        if (Cons.MainCategoryList_ary_list.size() > 0) {
            setData();
        } else {
            if (NetworkUtils.getConnectivityStatus(context) != 0) {
                getMainCategoryList();
            } else {
                createInfoDialog(context, "Alert", getString(R.string.alert_internet));
            }
        }
    }

    public void setuptoolbar(String title) {

        getSupportActionBar().setTitle(Html.fromHtml("<small><b>" + title + "</b></small>"));
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        shopping_drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.shopping_frame);
        switch (v.getId()) {
            case R.id.dashboard:
                shopping_drawer.closeDrawers();
                ReplaceFragement(new Dashboard(), "Dashboard");
                break;
            case R.id.change_password:
                shopping_drawer.closeDrawers();
                ReplaceFragement(new ChangePassword(), "Change Password");
                break;
            case R.id.shoppingorders:
                shopping_drawer.closeDrawers();
                goToActivity(context, MyOrders.class, null);
                break;
            case R.id.shoppingcontacts:
                shopping_drawer.closeDrawers();
                ReplaceFragement(new ContactUs(), "Contact Us");
                break;
            case R.id.logout:
                shopping_drawer.closeDrawers();
                if (logout.getText().toString().equalsIgnoreCase("Logout"))
                    logoutDialog(context, Login.class);
                else goToActivityWithFinish(context, Login.class, null);
                break;
            case R.id.share_app:
                shopping_drawer.closeDrawers();
                if (PreferencesManager.getInstance(context).getUserId().equalsIgnoreCase(""))
                    goToActivityWithFinish(context, Login.class, null);
                else
//                    createShareLink();
                    break;

            case R.id.ac_info:
                shopping_drawer.closeDrawers();
                if (PreferencesManager.getInstance(context).getUserId().equalsIgnoreCase(""))
                    goToActivityWithFinish(context, Login.class, null);
                else
                    goToActivity(context, UpdateProfile.class, null);
                break;
            case R.id.address_book:
                shopping_drawer.closeDrawers();
                if (PreferencesManager.getInstance(context).getUserId().equalsIgnoreCase(""))
                    goToActivityWithFinish(context, Login.class, null);
                else
                    goToActivity(context, ActivityAddressManager.class, null);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout shopping_drawer = findViewById(R.id.drawer_layout);
        if (shopping_drawer.isDrawerOpen(GravityCompat.START)) {
            shopping_drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.shopping_frame);
            if (f instanceof Dashboard) {
                if (!doubleBackToExitPressedOnce) {
                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
                } else {
                    finishAffinity();
                }
            } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            } else {
                ReplaceFragement(new Dashboard(), "Dashboard");
            }
        }
    }

    public void ReplaceFragement(Fragment setFragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.shopping_frame, setFragment, title);
//        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportActionBar().setTitle(Html.fromHtml("<small><b>" + title + "</b></small>"));
        transaction.commit();
    }

    void getMainCategoryList() {
        showLoading();
        Call<ResponseMainCategory> call = apiServices.getMainCategory();
        call.enqueue(new Callback<ResponseMainCategory>() {
            @Override
            public void onResponse(Call<ResponseMainCategory> call, Response<ResponseMainCategory> response) {
                hideLoading();
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    Cons.MainCategoryList_ary_list.clear();
                    Cons.MainCategoryList_ary_list = response.body().getLstmenu().get(0).getMainMenu();
                    dynamic_lo.removeAllViews();

                    for (int i = 0; i < Cons.MainCategoryList_ary_list.size(); i++) {
                        child = getLayoutInflater().inflate(R.layout.category_list, null, false);
                        TextView tv_cat_name = child.findViewById(R.id.tv_cat_name);
                        tv_cat_name.setText(Cons.MainCategoryList_ary_list.get(i).getMenu());
                        child.setOnClickListener(click);
                        child.setTag(Integer.toString(i));
                        dynamic_lo.addView(child);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseMainCategory> call, Throwable t) {
                hideLoading();
            }
        });

    }

    private void setData() {
        dynamic_lo.removeAllViews();
        for (int i = 0; i < Cons.MainCategoryList_ary_list.size(); i++) {
            child = getLayoutInflater().inflate(R.layout.category_list, null, false);
            TextView tv_cat_name = child.findViewById(R.id.tv_cat_name);
            tv_cat_name.setText(Cons.MainCategoryList_ary_list.get(i).getMenu());
            child.setOnClickListener(click);
            child.setTag(Integer.toString(i));
            dynamic_lo.addView(child);
        }
    }

    //    private Firebase
//    private void createShareLink() {
//        Log.e("main", "create link ");
//        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
//                .setLink(Uri.parse("https://www.blueappsoftware.com/"))
//                .setDomainUriPrefix("referearnpro.page.link")
//                // Open links with this app on Android
//                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
//                // Open links with com.example.ios on iOS
//                //.setIosParameters(new DynamicLink.IosParameters.Builder("com.example.ios").build())
//                .buildDynamicLink();
////click -- link -- google play store -- inistalled/ or not  ----
//        Uri dynamicLinkUri = dynamicLink.getUri();
//        Log.e("main", "  Long refer " + dynamicLink.getUri());
//        //   https://referearnpro.page.link?apn=blueappsoftware.referearnpro&link=https%3A%2F%2Fwww.blueappsoftware.com%2F
//        // apn  ibi link
//        // manuall link
//        String sharelinktext = "https://bodegacustomer.page.link/?" +
//                "link=http://api.bodegaindia.in/invitedby=" + PreferencesManager.getInstance(context).getLoginId() +
//                "&apn=" + getPackageName() +
//                "&st=" + "My Refer Link" +
//                "&sd=" + "Earn Money" +
//                "&si=" + "http://api.bodegaindia.in/images/logo-light.jpg";
//        // shorten the link
//        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
//                //.setLongLink(dynamicLink.getUri())
//                .setLongLink(Uri.parse(sharelinktext))  // manually
//                .buildShortDynamicLink()
//                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
//                    @Override
//                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
//                        if (task.isSuccessful()) {
//                            // Short link created
//                            Uri shortLink = task.getResult().getShortLink();
//                            Uri flowchartLink = task.getResult().getPreviewLink();
//                            Log.e("main ", "short link " + shortLink.toString());
//                            // share app dialog
//                            Intent intent = new Intent();
//                            intent.setAction(Intent.ACTION_SEND);
//                            intent.putExtra(Intent.EXTRA_TEXT, shortLink.toString());
//                            intent.setType("text/plain");
//                            startActivity(intent);
//                        } else {
//                            // Error
//                            // ...
//                            Log.e("main", " error " + task.getException());
//                        }
//                    }
//                });
//    }

}
