package com.bodegacustomer.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bodegacustomer.BuildConfig;
import com.bodegacustomer.R;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.model.request.RequestSignup;
import com.bodegacustomer.model.response.ResponseSignup;
import com.bodegacustomer.utils.LoggerUtil;
import com.bodegacustomer.utils.NetworkUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bodegacustomer.activity.Signup.inviteCode;
import static com.bodegacustomer.app.AppConfig.PAYLOAD_BUNDLE;

public class OTPVerification extends BaseActivity implements TextWatcher {

    @BindView(R.id.tv_verification_msg)
    TextView tvVerificationMsg;
    @BindView(R.id.et_otp1)
    EditText etOtp1;
    @BindView(R.id.et_otp2)
    EditText etOtp2;
    @BindView(R.id.et_otp3)
    EditText etOtp3;
    @BindView(R.id.et_otp4)
    EditText etOtp4;
    private Bundle bundle;
    private String enteredOTP = "";
    String random = "0000";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_activity);
        ButterKnife.bind(this);

        bundle = getIntent().getBundleExtra(PAYLOAD_BUNDLE);
        if (bundle != null) {
            tvVerificationMsg.setText(String.format("%s%s", getResources().getString(R.string.verification_text) + " ", bundle.getString("number")));
        }

        etOtp1.addTextChangedListener(this);
        etOtp2.addTextChangedListener(this);
        etOtp3.addTextChangedListener(this);
        etOtp4.addTextChangedListener(this);

        if (NetworkUtils.getConnectivityStatus(context) != 0) {
//            random = generatePin();
//            String msg = random + " is the OTP for registration, and is valid for 5 min. http://admin.bodegaindia.in/";
//            sendMsg(bundle.getString("number"), msg);
            showMessage("OTP is 0000");
        } else
            showMessage(getResources().getString(R.string.alert_internet));
    }

    @OnClick({R.id.img_back, R.id.tv_resend_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                Bundle param = new Bundle();
                param.putString("number", bundle.getString("number"));
                goToActivityWithFinish(context, Splash.class, param);
                break;
            case R.id.tv_resend_submit:
                showMessage("OTP is 0000");
//                showMessage("OTP has been sent again.");
//                random = generatePin();
//                String msg = random + " is the OTP for registration, and is valid for 5 min. http://admin.bodegaindia.in/";
//                sendMsg(bundle.getString("number"), msg);
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() == 1) {
            if (etOtp1.length() == 1) {
                etOtp2.requestFocus();
            }
            if (etOtp2.length() == 1) {
                etOtp3.requestFocus();
            }
            if (etOtp3.length() == 1) {
                etOtp4.requestFocus();
            }
            if (etOtp4.length() == 1) {
                enteredOTP = etOtp1.getText().toString() + etOtp2.getText().toString() + etOtp3.getText().toString() + etOtp4.getText().toString();
//                TODO LATER
                if (enteredOTP.equalsIgnoreCase(random)) {
                    PreferencesManager.getInstance(context).setContact(bundle.getString("number"));
                    getSignUp();
                } else {
                    showMessage("Invalid OTP");
                }
            }
        } else if (editable.length() == 0) {
            if (etOtp4.length() == 0) {
                etOtp3.requestFocus();
            }
            if (etOtp3.length() == 0) {
                etOtp2.requestFocus();
            }
            if (etOtp2.length() == 0) {
                etOtp1.requestFocus();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Bundle param = new Bundle();
        param.putString("number", bundle.getString("number"));
        goToActivityWithFinish(context, Splash.class, param);
    }

    private void getSignUp() {
        RequestSignup requestSignup = new RequestSignup();
        requestSignup.setContact(bundle.getString("number"));
        requestSignup.setCustomerName(bundle.getString("name"));
        requestSignup.setEmail(bundle.getString("email"));
        requestSignup.setSponsorCode(inviteCode);

        showLoading();
        if (BuildConfig.DEBUG)
            LoggerUtil.logItem(requestSignup);

        Call<ResponseSignup> call = apiServices.getSignUp(requestSignup);
        call.enqueue(new Callback<ResponseSignup>() {
            @Override
            public void onResponse(Call<ResponseSignup> call, Response<ResponseSignup> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                try {
                    if (response.body().getStatus().equalsIgnoreCase("0")) {
                        createRegDialog("Registration Successful", response.body().getErrorMessage());
                    } else
                        showMessage(response.body().getErrorMessage());
                } catch (Exception e) {
                    showMessage("Some Error");
                }
            }

            @Override
            public void onFailure(Call<ResponseSignup> call, Throwable t) {
                hideLoading();
            }
        });
    }

    public void createRegDialog(String title,
                                String msg) {
        androidx.appcompat.app.AlertDialog.Builder builder1 = new androidx.appcompat.app.AlertDialog.Builder(context);
        builder1.setTitle(title);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setNegativeButton(
                "OK",
                (dialog, id) -> {
                    goToActivityWithFinish(context, Login.class, null);
                    dialog.cancel();
                });

        androidx.appcompat.app.AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    void sendMsg(String mobile, String msg) {
        String url = BuildConfig.SMS_URL + mobile + "&message=" + msg;
        LoggerUtil.logItem(url);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, LoggerUtil::logItem, error -> {

        });
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) {
            }
        });
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);

    }
}
