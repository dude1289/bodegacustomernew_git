package com.bodegacustomer.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bodegacustomer.BuildConfig;
import com.bodegacustomer.R;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.model.request.RequestAddToCart;
import com.bodegacustomer.model.request.RequestForgetPassword;
import com.bodegacustomer.model.request.RequestLogin;
import com.bodegacustomer.model.response.ResponseAddToCart;
import com.bodegacustomer.model.response.ResponseForgetPassword;
import com.bodegacustomer.model.response.ResponseLogin;
import com.bodegacustomer.utils.LoggerUtil;
import com.bodegacustomer.utils.NetworkUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends BaseActivity {

    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.tv_signup)
    TextView tvSignup;
    private SignInButton googleSignInButton;
    private GoogleSignInClient googleSignInClient;
    LoginButton loginButton;
    private static final String EMAIL = "email";
    CallbackManager callbackManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        googleSignInButton = findViewById(R.id.sign_in_button);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(this, gso);
        googleSignInButton.setOnClickListener(v -> {
            Intent signInIntent = googleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, 101);
        });


        if (!PreferencesManager.getInstance(context).getUserId().equalsIgnoreCase("")) {
            goToActivityWithFinish(context, ContainerActivity.class, null);
        }

        String account = getColoredSpanned("You don't have Bodega Account?", "#000000");
        String signup = getColoredSpanned("<b>" + " Register Now" + "</b>", "#E2C61B");
        tvSignup.setText(Html.fromHtml(account + " " + signup));

        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        // Registering CallbackManager with the LoginButton
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // Retrieving access token using the LoginResult
                AccessToken accessToken = loginResult.getAccessToken();
                LoggerUtil.logItem("acc" + accessToken);
                useLoginInformation(accessToken);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
                LoggerUtil.logItem("Err" + error);
            }
        });

    }

    private void useLoginInformation(AccessToken accessToken) {
        /**
         Creating the GraphRequest to fetch user details
         1st Param - AccessToken
         2nd Param - Callback (which will be invoked once the request is successful)
         **/
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            //OnCompleted is invoked once the GraphRequest is successful
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String name = object.getString("name");
                    String email = object.getString("email");
                    String image = object.getJSONObject("picture").getJSONObject("data").getString("url");
                    LoggerUtil.logItem(name);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        // We set parameters to the GraphRequest using a Bundle.
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(200)");
        request.setParameters(parameters);
        // Initiate the GraphRequest
        request.executeAsync();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
        LoggerUtil.logItem(data + "," + requestCode + "," + resultCode);
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case 101:
                    try {
                        // The Task returned from this call is always completed, no need to attach
                        // a listener.
                        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                        GoogleSignInAccount account = task.getResult(ApiException.class);
                        onLoggedIn(account);
                        Log.e("Gsign", "logged");
                    } catch (ApiException e) {
                        // The ApiException status code indicates the detailed failure reason.
                        showMessage("Try Later");
                        Log.e("Gsign", "signInResult:failed code=" + e.getStatusCode());
                    }
                    break;
            }
    }

    private void onLoggedIn(GoogleSignInAccount googleSignInAccount) {
        Intent intent = new Intent(this, Signup.class);
        intent.putExtra(Signup.GOOGLE_ACCOUNT, googleSignInAccount);
        startActivity(intent);
        finish();
    }


    @OnClick({R.id.btn_login, R.id.tv_forget_password, R.id.tv_signup, R.id.tv_skip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (NetworkUtils.getConnectivityStatus(context) != 0) {
                    if (loginValidation()) {
                        getLogin();
                    }
                } else
                    showMessage(getResources().getString(R.string.alert_internet));
                break;
            case R.id.tv_forget_password:
                forgotPasswordDialog();
                break;
            case R.id.tv_signup:
                goToActivityWithFinish(context, Signup.class, null);
                break;
            case R.id.tv_skip:
                PreferencesManager.getInstance(context).setIsFirstTimeLaunch(false);
                goToActivityWithFinish(context, ContainerActivity.class, null);
                break;
        }
    }

    boolean loginValidation() {
        if (etUsername.getText().toString().length() < 1) {
            etUsername.setError("Enter Username");
            return false;
        } else if (etPassword.getText().toString().length() < 1) {
            etPassword.setError("Enter Password");
            etUsername.clearFocus();
            return false;
        }
        etUsername.clearFocus();
        etPassword.clearFocus();
        return true;
    }

    private void getLogin() {
        RequestLogin login = new RequestLogin();
        login.setLoginId(etUsername.getText().toString());
        login.setPassword(etPassword.getText().toString());

        LoggerUtil.logItem(login);
        showLoading();

        Call<ResponseLogin> call = apiServices.getLogin(login);
        call.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                hideLoading();
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    PreferencesManager.getInstance(context).setUserId(response.body().getFkCustId());
                    PreferencesManager.getInstance(context).setFull_Name(response.body().getCustomerName());
                    PreferencesManager.getInstance(context).setContact(response.body().getContact());
                    PreferencesManager.getInstance(context).setLoginId(etUsername.getText().toString().trim());
                    PreferencesManager.getInstance(context).setPassword(etPassword.getText().toString().trim());
                    PreferencesManager.getInstance(context).setEmail(response.body().getEmail());
                    PreferencesManager.getInstance(context).setIsFirstTimeLaunch(false);
                    if (!PreferencesManager.getInstance(context).getProducyInfoId().equalsIgnoreCase("")) {
                        addToCart();
                    } else
                        goToActivityWithFinish(context, ContainerActivity.class, null);
                } else showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void addToCart() {
        RequestAddToCart addToCart = new RequestAddToCart();
        addToCart.setProductInfoCode(PreferencesManager.getInstance(context).getProducyInfoId());
        addToCart.setCustomerId(PreferencesManager.getInstance(context).getUserId());
        addToCart.setFkVendorId(PreferencesManager.getInstance(context).getVendorId());
        addToCart.setProductQuantity("1");
        addToCart.setType("Add");

        showLoading();

        if (BuildConfig.DEBUG)
            LoggerUtil.logItem(addToCart);

        Call<ResponseAddToCart> call = apiServices.addToCart(addToCart);
        call.enqueue(new Callback<ResponseAddToCart>() {
            @Override
            public void onResponse(Call<ResponseAddToCart> call, Response<ResponseAddToCart> response) {
                hideLoading();
                if (BuildConfig.DEBUG)
                    LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    goToActivity(context, CartItems.class, null);
                }
                showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponseAddToCart> call, Throwable t) {
                hideLoading();
            }
        });
    }

    Dialog forgotpswdDialog;

    private void forgotPasswordDialog() {
        forgotpswdDialog = new Dialog(context);
        forgotpswdDialog.setCanceledOnTouchOutside(false);
        forgotpswdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgotpswdDialog.setContentView(R.layout.dialog_forgot_pswd);
        Objects.requireNonNull(forgotpswdDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);

        EditText et_mobile = forgotpswdDialog.findViewById(R.id.et_mobile);
        Button btn_cancel = forgotpswdDialog.findViewById(R.id.btn_cancel);
        Button btn_submit = forgotpswdDialog.findViewById(R.id.btn_submit);

        btn_cancel.setOnClickListener(view -> forgotpswdDialog.dismiss());

        btn_submit.setOnClickListener(view -> {
            if (et_mobile.getText().toString().length() == 10) {
                ForgetPass(et_mobile.getText().toString().trim());
            } else et_mobile.setError("Enter Mobile Number.");
        });

        forgotpswdDialog.setCancelable(false);
        forgotpswdDialog.setCanceledOnTouchOutside(false);
        forgotpswdDialog.show();
    }

    private void ForgetPass(String mobile) {
        hideKeyboard();
        showLoading();
        RequestForgetPassword password = new RequestForgetPassword();
        password.setMobileNo(mobile);

        LoggerUtil.logItem(password);
        Call<ResponseForgetPassword> call = apiServices.ForgetPass(password);
        call.enqueue(new Callback<ResponseForgetPassword>() {
            @Override
            public void onResponse(Call<ResponseForgetPassword> call, Response<ResponseForgetPassword> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0"))
                    forgotpswdDialog.dismiss();
                showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponseForgetPassword> call, Throwable t) {

            }
        });

    }
}