package com.bodegacustomer.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.BuildConfig;
import com.bodegacustomer.R;
import com.bodegacustomer.adapter.AddressAdapter;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.model.request.RequestCartItems;
import com.bodegacustomer.model.request.RequestPlaceOrder;
import com.bodegacustomer.model.request.RequestSaveAddress;
import com.bodegacustomer.model.response.ResponseCancelOrder;
import com.bodegacustomer.model.response.ResponsePlaceOrder;
import com.bodegacustomer.model.response.ResponseSaveAddress;
import com.bodegacustomer.model.response.ResponseWalletAmount;
import com.bodegacustomer.model.response.responseAddresses.ResponseGetAddress;
import com.bodegacustomer.model.response.stateCity.ResponseStateCity;
import com.bodegacustomer.retrofit.CancelOrderListener;
import com.bodegacustomer.utils.LoggerUtil;
import com.bodegacustomer.utils.NetworkUtils;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URL;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bodegacustomer.app.AppConfig.PAYLOAD_BUNDLE;

public class ActivityAddressManager extends BaseActivity implements PaymentResultListener, CancelOrderListener {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    @BindView(R.id.btn_pay)
    Button btnPay;
    public static String selectedAddressId = "";
    Dialog address_dialog, payment_dialog;
    NodeList nodelist;
    EditText et_city, et_state;
    String addressType = "Home", selectedPaymentMode = "Cash";
    String walletBalance = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addressess);
        ButterKnife.bind(this);

        imgCart.setVisibility(View.GONE);
        tvTitle.setText("Select Address");
        rvCategories.setLayoutManager(new LinearLayoutManager(context));

        if (NetworkUtils.getConnectivityStatus(context) != 0)
            getAddress();

        if (getIntent().getBundleExtra(PAYLOAD_BUNDLE) != null) {
            btnPay.setVisibility(View.VISIBLE);
            btnPay.setText(String.format("Proceed To Pay ( ₹%s)", getIntent().getBundleExtra(PAYLOAD_BUNDLE).getString("amount")));
        } else {
            btnPay.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.img_back, R.id.btn_add_address, R.id.btn_pay})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_add_address:
                addAddressDialog();
                break;
            case R.id.btn_pay:
                if (!selectedAddressId.equalsIgnoreCase(""))
                    psymentDialog();
                else showMessage("Select delivery address.");
                break;
        }
    }

    private void getAddress() {

        RequestCartItems items = new RequestCartItems();
        items.setCustomerId(PreferencesManager.getInstance(context).getUserId());

        LoggerUtil.logItem(items);
        showLoading();

        Call<ResponseGetAddress> call = apiServices.getAddress(items);
        call.enqueue(new Callback<ResponseGetAddress>() {
            @Override
            public void onResponse(Call<ResponseGetAddress> call, Response<ResponseGetAddress> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    rvCategories.setVisibility(View.VISIBLE);
                    AddressAdapter addressAdapter = new AddressAdapter(context, response.body().getLstaddress().get(0).getAddress(), ActivityAddressManager.this);
                    rvCategories.setAdapter(addressAdapter);
                } else {
                    rvCategories.setVisibility(View.GONE);
                    showMessage("No Address Found.");
                }
            }

            @Override
            public void onFailure(Call<ResponseGetAddress> call, Throwable t) {
                hideLoading();
            }
        });

    }

    private void addAddressDialog() {
        address_dialog = new Dialog(context);
        address_dialog.setCanceledOnTouchOutside(false);
        address_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        address_dialog.setContentView(R.layout.dialog_address);
        Objects.requireNonNull(address_dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);

        Button btn_cancel = address_dialog.findViewById(R.id.btn_cancel);
        Button btn_submit = address_dialog.findViewById(R.id.btn_submit);

        et_city = address_dialog.findViewById(R.id.et_city);
        et_state = address_dialog.findViewById(R.id.et_state);
        EditText et_landmark = address_dialog.findViewById(R.id.et_landmark);
        EditText et_area = address_dialog.findViewById(R.id.et_area);
        EditText et_customer_name = address_dialog.findViewById(R.id.et_customer_name);
        EditText et_customer_mobile = address_dialog.findViewById(R.id.et_customer_mobile);
        EditText et_address = address_dialog.findViewById(R.id.et_address);
        EditText et_pincode = address_dialog.findViewById(R.id.et_pincode);
        RadioGroup radioGroup = address_dialog.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener((radioGroup1, i) -> {
            if (i == R.id.rb_home)
                addressType = "Home";
            else addressType = "Work";
        });

        et_customer_name.setText(PreferencesManager.getInstance(context).getFull_Name());
        et_customer_mobile.setText(PreferencesManager.getInstance(context).getContact());

        et_pincode.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0 && s.length() == 6)
                    getStateCityViaPin(et_pincode.getText().toString().trim());
            }
        });

        btn_submit.setOnClickListener(view -> {
            if (et_area.getText().toString().length() < 0 ||
                    et_landmark.getText().toString().length() < 0 ||
                    et_address.getText().toString().length() < 0 ||
                    et_pincode.getText().toString().length() != 6) {
                showMessage("Enter all fields");
            } else
                saveAddress(et_customer_name.getText().toString(), et_customer_mobile.getText().toString().trim(), et_area.getText().toString(), et_address.getText().toString(), et_landmark.getText().toString(), et_pincode.getText().toString(), addressType);
        });

        btn_cancel.setOnClickListener(view -> address_dialog.dismiss());

        address_dialog.setCancelable(false);
        address_dialog.setCanceledOnTouchOutside(false);
        address_dialog.show();
    }

    private void saveAddress(String name, String contact, String house, String locality, String lanmark, String pincode, String addressType) {

        RequestSaveAddress address = new RequestSaveAddress();
        address.setAddressType(addressType);
        address.setCustomerId(PreferencesManager.getInstance(context).getUserId());
        address.setHouseNo(house);
        address.setLandMark(lanmark);
        address.setLocality(locality);
        address.setPinCode(pincode);
        address.setCustomerName(name);
        address.setContact(contact);

        LoggerUtil.logItem(address);
        showLoading();
        hideKeyboard();

        Call<ResponseSaveAddress> call = apiServices.saveAddress(address);
        call.enqueue(new Callback<ResponseSaveAddress>() {
            @Override
            public void onResponse(Call<ResponseSaveAddress> call, Response<ResponseSaveAddress> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    address_dialog.dismiss();
                    showMessage(response.body().getErrorMessage());
                    getAddress();
                } else showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponseSaveAddress> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void getStateCityViaPin(String pincode) {
        showLoading();
        JsonObject object = new JsonObject();
        object.addProperty("pincode", pincode);

        LoggerUtil.logItem(object);
        Call<ResponseStateCity> call = apiServices.getStateCityViaPin(object);
        call.enqueue(new Callback<ResponseStateCity>() {
            @Override
            public void onResponse(Call<ResponseStateCity> call, Response<ResponseStateCity> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    et_state.setText(response.body().getLststatecitydata().get(0).getLststatecitydetails().get(0).getState());
                    et_city.setText(response.body().getLststatecitydata().get(0).getLststatecitydetails().get(0).getCity());
                } else showMessage(response.body().getErrorMessage() + "");
            }

            @Override
            public void onFailure(Call<ResponseStateCity> call, Throwable t) {

            }
        });
    }

    private void psymentDialog() {
        payment_dialog = new Dialog(context);
        payment_dialog.setCanceledOnTouchOutside(false);
        payment_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        payment_dialog.setContentView(R.layout.dialog_payment_mode);
        Objects.requireNonNull(payment_dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);

        RadioGroup radioGroup = payment_dialog.findViewById(R.id.radioGroup);
        Button btn_pay = payment_dialog.findViewById(R.id.btn_pay);
        TextView tv_pay_amt = payment_dialog.findViewById(R.id.tv_pay_amt);
        ImageView img_close = payment_dialog.findViewById(R.id.img_close);

        img_close.setOnClickListener(view -> payment_dialog.dismiss());

        tv_pay_amt.setText(String.format("Payable Amount %s", getIntent().getBundleExtra(PAYLOAD_BUNDLE).getString("amount")));

        radioGroup.setOnCheckedChangeListener((radioGroup1, i) -> {
            if (i == R.id.rb_cod)
                selectedPaymentMode = "Cash";
            else selectedPaymentMode = "Online";
        });

        btn_pay.setOnClickListener(view -> {
            if (selectedPaymentMode.equalsIgnoreCase("Online")) {
                startPayment(getIntent().getBundleExtra(PAYLOAD_BUNDLE).getString("amount"));
            } else placeOrder(selectedPaymentMode);
        });

        payment_dialog.setCancelable(false);
        payment_dialog.setCanceledOnTouchOutside(false);
        payment_dialog.show();
    }

    private void getwalletbalance() {
        RequestCartItems items = new RequestCartItems();
        items.setCustomerId(PreferencesManager.getInstance(context).getUserId());

        LoggerUtil.logItem(items);
        showLoading();

        Call<ResponseWalletAmount> call = apiServices.getWalletBalance(items);
        call.enqueue(new Callback<ResponseWalletAmount>() {
            @Override
            public void onResponse(Call<ResponseWalletAmount> call, Response<ResponseWalletAmount> response) {
                hideLoading();
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    walletBalance = response.body().getBalance();
                    psymentDialog();
                }
            }

            @Override
            public void onFailure(Call<ResponseWalletAmount> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void placeOrder(String payMode) {
        payment_dialog.dismiss();
        RequestPlaceOrder order = new RequestPlaceOrder();
        order.setCustomerId(PreferencesManager.getInstance(context).getUserId());
        order.setFkAddressId(selectedAddressId);
        order.setPaymentMode(selectedPaymentMode);

        showLoading();
        LoggerUtil.logItem(order);

        Call<ResponsePlaceOrder> call = apiServices.placeOrder(order);
        call.enqueue(new Callback<ResponsePlaceOrder>() {
            @Override
            public void onResponse(Call<ResponsePlaceOrder> call, Response<ResponsePlaceOrder> response) {
                hideLoading();
                if (response.body().getStatus().equalsIgnoreCase("0")) {
//                    Todo
                    OrderPlacedDialog(response.body().getOrderNo());
                } else showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponsePlaceOrder> call, Throwable t) {
                hideLoading();
            }
        });

    }

    private void OrderPlacedDialog(String orderId) {
        Dialog success_dialog = new Dialog(context);
        success_dialog.setCanceledOnTouchOutside(false);
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        success_dialog.setContentView(R.layout.dialog_order_success);
        Objects.requireNonNull(success_dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);

        TextView tv_order_id = success_dialog.findViewById(R.id.tv_order_id);
        tv_order_id.setText(String.format("Order Id  = %s", orderId));

        Button btn_my_orders = success_dialog.findViewById(R.id.btn_my_orders);
        btn_my_orders.setOnClickListener(view -> {
            success_dialog.dismiss();
            goToActivityWithFinish(context, MyOrders.class, null);
        });

        success_dialog.setCancelable(false);
        success_dialog.setCanceledOnTouchOutside(false);
        success_dialog.show();
    }

    // Payment using Razorpay
    private void startPayment(String amount) {
        Checkout checkout = new Checkout();
        checkout.setImage(R.mipmap.ic_launcher_foreground
        );
        final Activity activity = this;
        float wholeamt = Float.valueOf(amount);
        wholeamt = wholeamt * 100;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("description", "Sabka Mart");
            jsonObject.put("currency", "INR");
            jsonObject.put("amount", wholeamt);
            jsonObject.put("payment_capture", true);

            JSONObject preFill = new JSONObject();
            preFill.put("email", PreferencesManager.getInstance(context).getEmail());
            preFill.put("contact", PreferencesManager.getInstance(context).getContact());
            jsonObject.put("prefill", preFill);

            checkout.open(activity, jsonObject);

            Log.e("CheckOutRazor", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_SHORT).show();
        placeOrder(s);
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void cancelOrder(String productId) {
        showLoading();
        JsonObject object = new JsonObject();
        object.addProperty("CustomerId", PreferencesManager.getInstance(context).getUserId());
        object.addProperty("CustomerOtherInfoId", productId);

        LoggerUtil.logItem(object);
        Call<ResponseCancelOrder> call = apiServices.DeleteAddress(object);
        call.enqueue(new Callback<ResponseCancelOrder>() {
            @Override
            public void onResponse(Call<ResponseCancelOrder> call, Response<ResponseCancelOrder> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0"))
                    getAddress();
                showMessage(response.body().getErrorMessage() + "");
            }

            @Override
            public void onFailure(Call<ResponseCancelOrder> call, Throwable t) {
                hideLoading();
            }
        });
    }
}
