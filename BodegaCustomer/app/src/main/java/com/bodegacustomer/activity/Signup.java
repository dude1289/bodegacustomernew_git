package com.bodegacustomer.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.bodegacustomer.R;
import com.bodegacustomer.constants.BaseActivity;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Signup extends BaseActivity {

    @BindView(R.id.et_mobile)
    TextInputEditText etMobile;
    @BindView(R.id.et_name)
    TextInputEditText etName;
    public static final String GOOGLE_ACCOUNT = "google_account";
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    public static String inviteCode = "bodega";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);
        ButterKnife.bind(this);

        setDataOnView();

    }

    private void setDataOnView() {
        try {
            GoogleSignInAccount googleSignInAccount = getIntent().getParcelableExtra(GOOGLE_ACCOUNT);
            etName.setText(googleSignInAccount.getDisplayName());
            etEmail.setText(googleSignInAccount.getEmail());
        } catch (Exception | Error e) {
        }
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
        String mobile = etMobile.getText().toString().trim();
        String name = etMobile.getText().toString().trim();
        if (mobile.length() != 10 && name.length() < 2) {
            showMessage("Enter Mobile/Name");
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("email", etEmail.getText().toString().trim());
            bundle.putString("name", etName.getText().toString().trim());
            bundle.putString("number", etMobile.getText().toString().trim());
            goToActivityWithFinish(context, OTPVerification.class, bundle);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToActivityWithFinish(context, Login.class, null);
    }
}
