package com.bodegacustomer.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.adapter.DashboardNewArrivals;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.model.request.RequestSearch;
import com.bodegacustomer.model.response.responseSearch.ResponseSearch;
import com.bodegacustomer.utils.LoggerUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySearch extends BaseActivity {

    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.rv_category_list)
    RecyclerView rvCategoryList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        rvCategoryList.setLayoutManager(new GridLayoutManager(context, 2));
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etSearch.getText().toString().length() > 2)
                    getSearch(etSearch.getText().toString().trim());
                else if (etSearch.getText().toString().length() == 0)
                    onBackPressed();
            }
        });
    }

    private void getSearch(String key) {
        RequestSearch products = new RequestSearch();
        products.setProductName(key);
        showLoading();
        hideKeyboard();
        LoggerUtil.logItem(products);

        Call<ResponseSearch> call = apiServices.getGlobalSearch(products);
        call.enqueue(new Callback<ResponseSearch>() {
            @Override
            public void onResponse(Call<ResponseSearch> call, Response<ResponseSearch> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    DashboardNewArrivals dashboardNewArrivals = new DashboardNewArrivals(context, response.body().getProductList().get(0).getNewarrivals());
                    rvCategoryList.setAdapter(dashboardNewArrivals);
                } else showMessage("No product found");
            }

            @Override
            public void onFailure(Call<ResponseSearch> call, Throwable t) {

            }
        });
    }
}
