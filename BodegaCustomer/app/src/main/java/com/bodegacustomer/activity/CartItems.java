package com.bodegacustomer.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.BuildConfig;
import com.bodegacustomer.R;
import com.bodegacustomer.adapter.CartAdapter;
import com.bodegacustomer.adapter.CouponAdapter;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.model.request.RequestAddToCart;
import com.bodegacustomer.model.request.RequestApplyCoupon;
import com.bodegacustomer.model.request.RequestCartItems;
import com.bodegacustomer.model.request.RequestRemoveCartItem;
import com.bodegacustomer.model.response.ResponseAddToCart;
import com.bodegacustomer.model.response.ResponseApplyCoupon;
import com.bodegacustomer.model.response.ResponseRemoveCartItem;
import com.bodegacustomer.model.response.responseCartItems.ResponseCartItems;
import com.bodegacustomer.model.response.responseCoupons.CouponListItem;
import com.bodegacustomer.model.response.responseCoupons.ResponseCoupons;
import com.bodegacustomer.retrofit.CartIncDecListener;
import com.bodegacustomer.retrofit.CouponListener;
import com.bodegacustomer.utils.LoggerUtil;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartItems extends BaseActivity implements CartIncDecListener, CouponListener {

    @BindView(R.id.btn_check_out)
    Button btnCheckOut;
    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.tv_coupon)
    TextView tvCoupon;
    @BindView(R.id.tv_delivery_charges)
    TextView tvDeliveryCharges;
    @BindView(R.id.tv_payable_amt)
    TextView tvPayableAmt;
    Double payAmount = 0.00, deliveryCharges = 0.00;
    CartAdapter adapter;
    @BindView(R.id.cv_empty)
    ConstraintLayout cvEmpty;
    @BindView(R.id.cv_found)
    ConstraintLayout cvFound;

    //    Coupon Widgets
    EditText coupon_code;
    TextView tv_apply_coupon;

    boolean isCouponApplied = false;
    float couponAmount = 0;

    //    Coupon dialog
    RecyclerView recycler_couponlist;
    @BindView(R.id.tv_coupon_amt)
    TextView tvCouponAmt;
    private BottomSheetDialog couponDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        tv_apply_coupon = findViewById(R.id.tv_apply_coupon);
        coupon_code = findViewById(R.id.coupon_code);

        imgCart.setVisibility(View.GONE);
        tvTitle.setText("Cart");
        rvCategories.setLayoutManager(new LinearLayoutManager(context));
        getCartItems();
    }

    @OnClick({R.id.btn_check_out, R.id.img_back, R.id.btn_continue_shopping, R.id.show_coupons})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_check_out:
                Bundle bundle = new Bundle();
                bundle.putString("amount", String.valueOf(payAmount));
                goToActivity(context, ActivityAddressManager.class, bundle);
                break;
            case R.id.img_back:
                goToActivity(context, ContainerActivity.class, null);
                break;
            case R.id.btn_continue_shopping:
                goToActivityWithFinish(context, ContainerActivity.class, null);
                break;
            case R.id.show_coupons:
                GetCoupons();
                break;
        }
    }

    public void showBottomSheetDialog(List<CouponListItem> couponList) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_coupon, null);
        couponDialog = new BottomSheetDialog(this);
        recycler_couponlist = view.findViewById(R.id.recycler_couponlist);
        recycler_couponlist.setHasFixedSize(true);
        recycler_couponlist.setLayoutManager(new LinearLayoutManager(this));
        couponDialog.setContentView(view);
        CouponAdapter adapter = new CouponAdapter(getApplicationContext(), couponList, CartItems.this);
        recycler_couponlist.setAdapter(adapter);
        couponDialog.show();
    }

    private void getCartItems() {
        RequestCartItems items = new RequestCartItems();
        items.setCustomerId(PreferencesManager.getInstance(context).getUserId());

        showLoading();
        Call<ResponseCartItems> call = apiServices.getCartItems(items);
        call.enqueue(new Callback<ResponseCartItems>() {
            @Override
            public void onResponse(Call<ResponseCartItems> call, Response<ResponseCartItems> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    cvEmpty.setVisibility(View.GONE);
                    cvFound.setVisibility(View.VISIBLE);
                    payAmount = 0.00;
                    deliveryCharges = 0.00;
                    for (int i = 0; i < response.body().getLstcart().get(0).getCartDetails().size(); i++) {
                        payAmount = payAmount + Double.parseDouble(response.body().getLstcart().get(0).getCartDetails().get(i).getSubTotal());
                        deliveryCharges = deliveryCharges + Double.parseDouble(response.body().getLstcart().get(0).getCartDetails().get(i).getDeliveryCharge());
                    }
                    payAmount = payAmount + deliveryCharges;
                    tvDeliveryCharges.setText(String.valueOf(deliveryCharges));
                    tvPayableAmt.setText(String.valueOf(payAmount));
                    adapter = new CartAdapter(context, response.body().getLstcart().get(0).getCartDetails(), CartItems.this);
                    rvCategories.setAdapter(adapter);

                } else {
                    cvEmpty.setVisibility(View.VISIBLE);
                    cvFound.setVisibility(View.GONE);
//                    adapter.notifyDataSetChanged();
                    rvCategories.setVisibility(View.GONE);
                    showMessage(response.body().getErrorMessage() + "");
                }
            }

            @Override
            public void onFailure(Call<ResponseCartItems> call, Throwable t) {
                hideLoading();
            }
        });
    }

    @Override
    public void getCardItemQuantity(String productInfo, String vendorId, String type) {
        addToCart(productInfo, vendorId, type);
    }

    private void addToCart(String producyInfoId, String vendorId, String type) {
        RequestAddToCart addToCart = new RequestAddToCart();
        addToCart.setProductInfoCode(producyInfoId);
        addToCart.setCustomerId(PreferencesManager.getInstance(context).getUserId());
        addToCart.setFkVendorId(vendorId);
        addToCart.setProductQuantity("1");
        addToCart.setType(type);

        showLoading();
        if (BuildConfig.DEBUG)
            LoggerUtil.logItem(addToCart);

        Call<ResponseAddToCart> call = apiServices.addToCart(addToCart);
        call.enqueue(new Callback<ResponseAddToCart>() {
            @Override
            public void onResponse(Call<ResponseAddToCart> call, Response<ResponseAddToCart> response) {
                hideLoading();
                if (BuildConfig.DEBUG)
                    LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    getCartItems();
                } else adapter.notifyDataSetChanged();
                showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponseAddToCart> call, Throwable t) {
                hideLoading();
            }
        });
    }

    @Override
    public void removeCartItem(String cartId) {
        removeItem(cartId);
    }

    private void removeItem(String cartId) {
        RequestRemoveCartItem item = new RequestRemoveCartItem();
        item.setCartId(cartId);
        item.setCustomerId(PreferencesManager.getInstance(context).getUserId());

        showLoading();
        LoggerUtil.logItem(item);

        Call<ResponseRemoveCartItem> call = apiServices.removeItemFromCart(item);
        call.enqueue(new Callback<ResponseRemoveCartItem>() {
            @Override
            public void onResponse(Call<ResponseRemoveCartItem> call, Response<ResponseRemoveCartItem> response) {
                hideLoading();
                if (BuildConfig.DEBUG)
                    LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    getCartItems();
                }
                showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponseRemoveCartItem> call, Throwable t) {
                hideLoading();
            }
        });
    }

    @Override
    public void onBackPressed() {
        goToActivity(context, ContainerActivity.class, null);
    }

    @Override
    public void onCouponSelected(String couponCode, String pkId, String amt) {
        couponDialog.dismiss();
        ApplyCouponCode(couponCode, pkId, amt);
    }

    private void GetCoupons() {
        showLoading();
        JsonObject object = new JsonObject();
        object.addProperty("PK_UserId", PreferencesManager.getInstance(context).getUserId());

        Call<ResponseCoupons> call = apiServices.GetCoupons(object);
        call.enqueue(new Callback<ResponseCoupons>() {
            @Override
            public void onResponse(Call<ResponseCoupons> call, Response<ResponseCoupons> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    showBottomSheetDialog(response.body().getCoupon().get(0).getCouponList());
                } else showMessage("No Coupons Found");
            }

            @Override
            public void onFailure(Call<ResponseCoupons> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void ApplyCouponCode(String couponCode, String pkId, String amt) {
        showLoading();
        RequestApplyCoupon coupon = new RequestApplyCoupon();
        coupon.setCouponCode(couponCode);
        coupon.setFKCustomerID(pkId);

        LoggerUtil.logItem(coupon);
        Call<ResponseApplyCoupon> call = apiServices.ApplyCouponCode(coupon);
        call.enqueue(new Callback<ResponseApplyCoupon>() {
            @Override
            public void onResponse(Call<ResponseApplyCoupon> call, Response<ResponseApplyCoupon> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    coupon_code.setText(couponCode);
                    showMessage("Applied Successfully");
                    payAmount = payAmount - Double.parseDouble(amt);
                    tvPayableAmt.setText(String.valueOf(payAmount));
                    tvCouponAmt.setText(amt);
                } else showMessage(response.body().getErrorMessage());
            }

            @Override
            public void onFailure(Call<ResponseApplyCoupon> call, Throwable t) {
                hideLoading();
            }
        });
    }
}
