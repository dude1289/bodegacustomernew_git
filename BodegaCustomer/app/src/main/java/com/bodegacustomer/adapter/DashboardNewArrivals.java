package com.bodegacustomer.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.activity.ProductDetails;
import com.bodegacustomer.constants.BaseActivity;
import com.bodegacustomer.model.response.responseDashboard.NewarrivalsItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.engine.DiskCacheStrategy.AUTOMATIC;

public class DashboardNewArrivals extends RecyclerView.Adapter<DashboardNewArrivals.MyViewHolder> {
    private Context context;
    private List<NewarrivalsItem> productList;

    public DashboardNewArrivals(Context mContext, List<NewarrivalsItem> productList) {
        this.context = mContext;
        this.productList = productList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_products_dashboard, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.itemtitle.setText(productList.get(position).getProductName());
        holder.itemprice.setText(String.format("₹ %s", productList.get(position).getOfferedPrice()));
        holder.itemmrp.setText(String.format("₹ %s", productList.get(position).getMRP()));
        holder.itemmrp.setPaintFlags(holder.itemmrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        holder.itemprice.setText(String.format("₹ %s", productList.get(position).get()));

        Glide.with(context).load("http://admin.bodegaindia.in" + (productList.get(position).getImages()).replace("..", ""))
                .apply(new RequestOptions().diskCacheStrategy(AUTOMATIC).placeholder(R.drawable.logo).error(R.drawable.logo))
                .into(holder.itemthumbnail);

        if (productList.get(position).getSoldOutCss().equalsIgnoreCase("soldout"))
            holder.tv_sold_out.setVisibility(View.VISIBLE);
        else holder.tv_sold_out.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.itemthumbnail)
        ImageView itemthumbnail;
        @BindView(R.id.itemtitle)
        TextView itemtitle;
        @BindView(R.id.itemprice)
        TextView itemprice;
        @BindView(R.id.itemmrp)
        TextView itemmrp;
        @BindView(R.id.tv_sold_out)
        TextView tv_sold_out;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            itemthumbnail.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.itemthumbnail:
                    if (productList.get(getAdapterPosition()).getSoldOutCss().equalsIgnoreCase("")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("productId", productList.get(getAdapterPosition()).getPkProductId());
                        ((BaseActivity) context).goToActivity((Activity) context, ProductDetails.class, bundle);
                    }
                    break;
            }
        }
    }

}
