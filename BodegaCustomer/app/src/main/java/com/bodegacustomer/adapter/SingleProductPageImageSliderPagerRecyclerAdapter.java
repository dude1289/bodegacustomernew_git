package com.bodegacustomer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bodegacustomer.R;
import com.bodegacustomer.app.AppController;
import com.bodegacustomer.model.response.responseProductDetails.LstimagesItem;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Abhishek on 24/2/17.
 */

public class SingleProductPageImageSliderPagerRecyclerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater inflater;
    List<LstimagesItem> imageList;

    public SingleProductPageImageSliderPagerRecyclerAdapter(Context context, List<LstimagesItem> imageList) {
        try {

            this.context = context;
            this.imageList = imageList;
            //this.ints = ints;
            this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {


        }

    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = inflater.inflate(R.layout.image_slide_layout, container, false);

        NetworkImageView imageView = itemView.findViewById(R.id.img_view);
        try {
            if (imageList.get(position).getImagePath().length() > 0) {
                imageView.setImageUrl("http://admin.bodegaindia.in" + (imageList.get(position).getImagePath()).replace("..", ""), AppController.getInstance().getImageLoader());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        imageView.setDefaultImageResId(R.drawable.logo);
        imageView.setErrorImageResId(R.drawable.logo);

        container.addView(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                JSONObject jsonObject = new JSONObject();
                JSONArray jsonArray = new JSONArray();

                try {
                    for (LstimagesItem item : imageList) {

                        JSONObject object = new JSONObject();
                        object.put("url", "http://admin.bodegaindia.in" + (item.getImagePath()).replace("..", ""));

                        jsonArray.put(object);

                    }

                    jsonObject.put("jsonArray", jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                Intent intent = new Intent(context, FullScreenImageActivity.class);
//                intent.putExtra("jsonObject", jsonObject.toString());
//                intent.putExtra("setSelection", 0);
//                context.startActivity(intent);

            }
        });

        return itemView;
    }

    public View getTabView(int position) {

        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        NetworkImageView icon = view.findViewById(R.id.icon);
        try {
            icon.setImageUrl("http://admin.bodegaindia.in" + (imageList.get(position).getImagePath()).replace("..", ""), AppController.getInstance().getImageLoader());
        }catch (Exception e){
        }

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
