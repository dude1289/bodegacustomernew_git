package com.bodegacustomer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.model.response.responseSubCategort.CategoryDetailsItem;
import com.bodegacustomer.retrofit.SubCategoryList;
import com.bodegacustomer.retrofit.SubCategoryListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainCategoryAdapter extends RecyclerView.Adapter<MainCategoryAdapter.MyViewHolder> {
    private Context context;
    private List<CategoryDetailsItem> productList;
    SubCategoryListener subCategoryListener;

    SubCategoryList listener;

    public MainCategoryAdapter(Context mContext, List<CategoryDetailsItem> productList,
                               SubCategoryList listener, SubCategoryListener subCategoryListener) {
        this.context = mContext;
        this.productList = productList;
        this.listener = listener;
        this.subCategoryListener = subCategoryListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_category_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.tvCategory.setText(productList.get(position).getCategory());

        holder.tvCategory.setOnClickListener(view -> {
            listener.getSubCategoryList(productList.get(position).getSubcategorylst());
            subCategoryListener.getSubCategoryId(productList.get(position).getPKCategoryID(), true);
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_cat_name)
        TextView tvCategory;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}