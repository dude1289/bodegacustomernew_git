package com.bodegacustomer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.model.response.responseCoupons.CouponListItem;
import com.bodegacustomer.retrofit.CouponListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.CartViewHolder> {
    private Context context;
    private List<CouponListItem> list;
    private CouponListener listener;

    public CouponAdapter(Context context, List<CouponListItem> list, CouponListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_coupon, parent, false);
        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartViewHolder holder, final int position) {

        holder.tvCouponCode.setText(list.get(position).getCouponCode());
//        holder.tvMinAmt.setText("Min. Amount: ₹ " + list.get(position).getMinAmount());
        holder.tvMaxDiscount.setText("Max. Discount: ₹ " + list.get(position).getDiscountAmount());
        holder.tvValidity.setText("Valid Up-to: " + (list.get(position).getValidTo()));

        holder.btnApply.setOnClickListener(view -> listener.onCouponSelected(list.get(position).getCouponCode(), list.get(position).getPKDiscForCustID(), list.get(position).getDiscountAmount()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder {
        Button btnApply;
        TextView tvValidity;
        TextView tvMinAmt;
        TextView tvMaxDiscount;
        TextView tvCouponCode;

        public CartViewHolder(@NonNull View itemView) {
            super(itemView);
            btnApply = itemView.findViewById(R.id.btn_apply);
            tvValidity = itemView.findViewById(R.id.tv_validity);
            tvMinAmt = itemView.findViewById(R.id.tv_min_amt);
            tvMaxDiscount = itemView.findViewById(R.id.tv_max_discount);
            tvCouponCode = itemView.findViewById(R.id.tv_coupon_code);
        }
    }

    private static String changeDateFormat(String date) {
//        2019-09-30
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM, yy", Locale.ENGLISH);
        return timeFormat.format(myDate);
    }
}

