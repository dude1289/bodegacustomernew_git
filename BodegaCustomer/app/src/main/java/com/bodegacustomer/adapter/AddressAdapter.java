package com.bodegacustomer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.model.response.responseAddresses.AddressItem;
import com.bodegacustomer.retrofit.CancelOrderListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bodegacustomer.activity.ActivityAddressManager.selectedAddressId;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {

    private Context context;
    private List<AddressItem> productList;
    CancelOrderListener listener;

    public AddressAdapter(Context mContext, List<AddressItem> productList, CancelOrderListener listener) {
        this.context = mContext;
        this.productList = productList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_address, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.tvName.setText(productList.get(position).getCustomerName());
        holder.tvContact.setText(productList.get(position).getContact());
        holder.imgSelect.setImageResource((R.drawable.ic_check));
        holder.tvAddress.setText(String.format("%s, Near %s, %s, %s (%s)", productList.get(position).getHouseNo(), productList.get(position).getLocality(), productList.get(position).getLandMark(), productList.get(position).getPinCode(), productList.get(position).getAddressType()));

        holder.imgSelect.setOnClickListener(view -> {
            selectedAddressId = productList.get(position).getPk_AddressId();
            notifyDataSetChanged();
            new Handler().postDelayed(() -> holder.imgSelect.setImageResource((R.drawable.ic_checked)), 200);
        });

        holder.imgDelete.setOnClickListener(v -> listener.cancelOrder(productList.get(position).getPk_AddressId()));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.tv_contact)
        TextView tvContact;
        @BindView(R.id.img_select)
        ImageView imgSelect;
        @BindView(R.id.img_delete)
        ImageView imgDelete;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}


