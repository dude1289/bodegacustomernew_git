package com.bodegacustomer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.model.response.responseSubCategort.CategoryDetailsItem;
import com.bodegacustomer.retrofit.SubCategoryListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.MyViewHolder> {
    private Context context;
    private List<CategoryDetailsItem> productList;
    SubCategoryListener listener;

    public AdapterCategory(Context mContext, List<CategoryDetailsItem> productList, SubCategoryListener listener) {
        this.context = mContext;
        this.productList = productList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_category, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.tvCategory.setText(productList.get(position).getCategory());
        if (productList.get(position).getSubcategorylst().size() > 0)
            holder.tvCategory.setVisibility(View.VISIBLE);
        else holder.tvCategory.setVisibility(View.GONE);
        AdapterSubCategory adapterSubCategory = new AdapterSubCategory(context, productList.get(position).getSubcategorylst(), listener);
        holder.rvSubCategory.setAdapter(adapterSubCategory);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_category)
        TextView tvCategory;
        @BindView(R.id.rv_sub_category)
        RecyclerView rvSubCategory;


        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            rvSubCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        }
    }

}
