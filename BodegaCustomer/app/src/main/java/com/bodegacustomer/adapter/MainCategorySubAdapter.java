package com.bodegacustomer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.model.response.responseSubCategort.SubcategorylstItem;
import com.bodegacustomer.retrofit.SubCategoryListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainCategorySubAdapter extends RecyclerView.Adapter<MainCategorySubAdapter.MyViewHolder> {
    private Context context;
    private List<SubcategorylstItem> productList;
    SubCategoryListener listener;

    public MainCategorySubAdapter(Context mContext, List<SubcategorylstItem> productList, SubCategoryListener listener) {
        this.context = mContext;
        this.productList = productList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_main_sub_category, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.tvCategory.setText(productList.get(position).getSubCategoryName());
        Glide.with(context).load("http://admin.bodegaindia.in" + (productList.get(position).getImagePath()).replace("..", ""))
                .apply(RequestOptions.circleCropTransform())
                .into(holder.img_category);

        holder.itemView.setOnClickListener(view -> listener.getSubCategoryId(productList.get(position).getPKSubCategoryID(), false));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_category)
        TextView tvCategory;
        @BindView(R.id.img_category)
        ImageView img_category;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}