package com.bodegacustomer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.model.response.responseSubCategort.SubcategorylstItem;
import com.bodegacustomer.retrofit.SubCategoryListener;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterSubCategory extends RecyclerView.Adapter<AdapterSubCategory.MyViewHolder> {

    private Context context;
    private List<SubcategorylstItem> productList;
    SubCategoryListener listener;

    public AdapterSubCategory(Context mContext, List<SubcategorylstItem> productList, SubCategoryListener listener) {
        this.context = mContext;
        this.productList = productList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_sub_category, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.tvCategory.setText(productList.get(position).getSubCategoryName());
        holder.cv_sub_category.setOnClickListener(view -> listener.getSubCategoryId(productList.get(position).getPKSubCategoryID(), false));

        holder.img_category.setVisibility(View.GONE);
        Glide.with(context).load("http://admin.bodegaindia.in" + (productList.get(position).getImagePath()).replace("..", ""))
                .into(holder.img_category);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_category)
        TextView tvCategory;
        @BindView(R.id.img_category)
        ImageView img_category;
        @BindView(R.id.cv_sub_category)
        ConstraintLayout cv_sub_category;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}