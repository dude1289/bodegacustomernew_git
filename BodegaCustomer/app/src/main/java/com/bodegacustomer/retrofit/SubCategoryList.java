package com.bodegacustomer.retrofit;

import com.bodegacustomer.model.response.responseSubCategort.SubcategorylstItem;

import java.util.List;

public interface SubCategoryList {

    void getSubCategoryList(List<SubcategorylstItem> subcategorylst);
}
