package com.bodegacustomer.retrofit;

public interface CouponListener {

    void onCouponSelected(String couponCode, String pkId, String amt);
}
