package com.bodegacustomer.retrofit;

public interface CartIncDecListener {

    void getCardItemQuantity(String productInfo, String vendorId, String type);

    void removeCartItem(String cartId);
}
