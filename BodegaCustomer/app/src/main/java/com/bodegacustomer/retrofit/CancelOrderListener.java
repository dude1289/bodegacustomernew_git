package com.bodegacustomer.retrofit;

public interface CancelOrderListener {
    void cancelOrder(String productId);
}
