package com.bodegacustomer.retrofit;

public interface SelectedVarientsListener {

    void selectedVarients(String size, String color, String flavour, String ram, String Material, String storage);
}
