package com.bodegacustomer.retrofit;

public interface SubCategoryListener {
    void getSubCategoryId(String categoryId, boolean isMAin);
}
