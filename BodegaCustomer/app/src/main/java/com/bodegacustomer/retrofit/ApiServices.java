package com.bodegacustomer.retrofit;

import com.bodegacustomer.model.request.RequestAddToCart;
import com.bodegacustomer.model.request.RequestApplyCoupon;
import com.bodegacustomer.model.request.RequestCancelOrder;
import com.bodegacustomer.model.request.RequestCartItems;
import com.bodegacustomer.model.request.RequestChangePassword;
import com.bodegacustomer.model.request.RequestForgetPassword;
import com.bodegacustomer.model.request.RequestLogin;
import com.bodegacustomer.model.request.RequestPlaceOrder;
import com.bodegacustomer.model.request.RequestProductDetails;
import com.bodegacustomer.model.request.RequestRemoveCartItem;
import com.bodegacustomer.model.request.RequestSaveAddress;
import com.bodegacustomer.model.request.RequestSearch;
import com.bodegacustomer.model.request.RequestSignup;
import com.bodegacustomer.model.request.RequestSubCategoryProducts;
import com.bodegacustomer.model.request.RequestUpdateProfile;
import com.bodegacustomer.model.response.ResponseAddToCart;
import com.bodegacustomer.model.response.ResponseApplyCoupon;
import com.bodegacustomer.model.response.ResponseCancelOrder;
import com.bodegacustomer.model.response.ResponseCartCount;
import com.bodegacustomer.model.response.ResponseChangePassword;
import com.bodegacustomer.model.response.ResponseForgetPassword;
import com.bodegacustomer.model.response.ResponseLogin;
import com.bodegacustomer.model.response.ResponsePlaceOrder;
import com.bodegacustomer.model.response.ResponseRemoveCartItem;
import com.bodegacustomer.model.response.ResponseSaveAddress;
import com.bodegacustomer.model.response.ResponseSignup;
import com.bodegacustomer.model.response.ResponseUpdateProfile;
import com.bodegacustomer.model.response.ResponseWalletAmount;
import com.bodegacustomer.model.response.responseAddresses.ResponseGetAddress;
import com.bodegacustomer.model.response.responseBanner.ResponseBanner;
import com.bodegacustomer.model.response.responseCartItems.ResponseCartItems;
import com.bodegacustomer.model.response.responseCoupons.ResponseCoupons;
import com.bodegacustomer.model.response.responseDashboard.ResponseDashboard;
import com.bodegacustomer.model.response.responseMainCategory.ResponseMainCategory;
import com.bodegacustomer.model.response.responseOrders.ResponseOrders;
import com.bodegacustomer.model.response.responseProductDetails.ResponseProductDetails;
import com.bodegacustomer.model.response.responseSearch.ResponseSearch;
import com.bodegacustomer.model.response.responseSubCategort.ResponseSubCategory;
import com.bodegacustomer.model.response.responseSubCategoryProducts.ResponseSubCategoryProducts;
import com.bodegacustomer.model.response.stateCity.ResponseStateCity;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Abhishek on 29/6/18.
 */

public interface ApiServices {

    @POST("Login")
    Call<ResponseLogin> getLogin(@Body RequestLogin jsonObject);

    @POST("GetMenu")
    Call<ResponseMainCategory> getMainCategory();

    @POST("GetBanner")
    Call<ResponseBanner> getBanner();

    @POST("GetDashboardData")
    Call<ResponseDashboard> GetDashboardData(@Body JsonObject object);

    @POST("GetSubMenu")
    Call<ResponseSubCategory> GetMenu(@Body JsonObject data);

    @POST("GetCategory")
    Call<ResponseSubCategory> getMenuSubMenu();

    @POST("SignUp")
    Call<ResponseSignup> getSignUp(@Body RequestSignup data);

    @POST("QuickView")
    Call<ResponseProductDetails> getQuickView(@Body RequestProductDetails data);

    @POST("AddToCart")
    Call<ResponseAddToCart> addToCart(@Body RequestAddToCart data);

    @POST("ShowCart")
    Call<ResponseCartItems> getCartItems(@Body RequestCartItems data);

    @POST("RemoveItemFromCart")
    Call<ResponseRemoveCartItem> removeItemFromCart(@Body RequestRemoveCartItem data);

    @POST("GetAddress")
    Call<ResponseGetAddress> getAddress(@Body RequestCartItems data);

    @POST("SaveAddress")
    Call<ResponseSaveAddress> saveAddress(@Body RequestSaveAddress data);

    @POST("GetWalletBalance")
    Call<ResponseWalletAmount> getWalletBalance(@Body RequestCartItems data);

    @POST("PlaceOrder")
    Call<ResponsePlaceOrder> placeOrder(@Body RequestPlaceOrder data);

    @POST("MyOrders")
    Call<ResponseOrders> myOrders(@Body RequestCartItems data);

    @POST("CancelOrder")
    Call<ResponseCancelOrder> cancelOrder(@Body RequestCancelOrder data);

    @POST("CartCount")
    Call<ResponseCartCount> cartCount(@Body RequestCartItems data);

    @POST("GetProductByMenu")
    Call<ResponseSubCategoryProducts> getProductByMenu(@Body RequestSubCategoryProducts data);

    @POST("GlobalSearch")
    Call<ResponseSearch> getGlobalSearch(@Body RequestSearch data);

    @POST("ChangePassword")
    Call<ResponseChangePassword> getChangePassword(@Body RequestChangePassword data);

    @POST("GetCoupons")
    Call<ResponseCoupons> GetCoupons(@Body JsonObject data);

    @POST("ApplyCouponCode")
    Call<ResponseApplyCoupon> ApplyCouponCode(@Body RequestApplyCoupon data);

    @POST("UpdateProfile")
    Call<ResponseUpdateProfile> UpdateProfile(@Body RequestUpdateProfile data);

    @POST("ForgetPass")
    Call<ResponseForgetPassword> ForgetPass(@Body RequestForgetPassword data);

    @POST("getStateCityViaPin")
    Call<ResponseStateCity> getStateCityViaPin(@Body JsonObject data);

    @POST("DeleteAddress")
    Call<ResponseCancelOrder> DeleteAddress(@Body JsonObject data);

}
