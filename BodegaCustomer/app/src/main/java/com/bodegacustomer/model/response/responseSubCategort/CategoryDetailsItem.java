package com.bodegacustomer.model.response.responseSubCategort;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryDetailsItem{

	@SerializedName("Category")
	private String category;

	@SerializedName("PK_CategoryID")
	private String pKCategoryID;

	@SerializedName("Subcategorylst")
	private List<SubcategorylstItem> subcategorylst;

	public String getCategory(){
		return category;
	}

	public String getPKCategoryID(){
		return pKCategoryID;
	}

	public List<SubcategorylstItem> getSubcategorylst(){
		return subcategorylst;
	}
}