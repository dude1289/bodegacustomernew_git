package com.bodegacustomer.model.response.responseSubCategort;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryListItem{

	@SerializedName("CategoryDetails")
	private List<CategoryDetailsItem> categoryDetails;

	public List<CategoryDetailsItem> getCategoryDetails(){
		return categoryDetails;
	}
}