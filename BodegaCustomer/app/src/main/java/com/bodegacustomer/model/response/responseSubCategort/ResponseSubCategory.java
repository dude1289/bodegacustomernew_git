package com.bodegacustomer.model.response.responseSubCategort;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseSubCategory{

	@SerializedName("Status")
	private String status;

	@SerializedName("ProductList")
	private List<ProductListItem> productList;

	@SerializedName("FK_MainCategory")
	private Object fKMainCategory;

	@SerializedName("CategoryList")
	private List<CategoryListItem> categoryList;

	public String getStatus(){
		return status;
	}

	public List<ProductListItem> getProductList(){
		return productList;
	}

	public Object getFKMainCategory(){
		return fKMainCategory;
	}

	public List<CategoryListItem> getCategoryList(){
		return categoryList;
	}
}