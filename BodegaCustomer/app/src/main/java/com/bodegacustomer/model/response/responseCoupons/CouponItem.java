package com.bodegacustomer.model.response.responseCoupons;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CouponItem{

	@SerializedName("CouponList")
	private List<CouponListItem> couponList;

	@SerializedName("Title")
	private String title;

	public List<CouponListItem> getCouponList(){
		return couponList;
	}

	public String getTitle(){
		return title;
	}
}