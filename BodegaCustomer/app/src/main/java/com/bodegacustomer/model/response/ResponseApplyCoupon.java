package com.bodegacustomer.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseApplyCoupon{

	@SerializedName("Status")
	private String status;

	@SerializedName("CouponCode")
	private String couponCode;

	@SerializedName("FK_CustomerID")
	private String fKCustomerID;

	@SerializedName("ErrorMessage")
	private String errorMessage;

	public String getStatus(){
		return status;
	}

	public String getCouponCode(){
		return couponCode;
	}

	public String getFKCustomerID(){
		return fKCustomerID;
	}

	public String getErrorMessage(){
		return errorMessage;
	}
}