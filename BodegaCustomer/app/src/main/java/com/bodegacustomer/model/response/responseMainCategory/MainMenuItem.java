package com.bodegacustomer.model.response.responseMainCategory;

import com.google.gson.annotations.SerializedName;

public class MainMenuItem{

	@SerializedName("FK_MainCategory")
	private String fKMainCategory;

	@SerializedName("Menu")
	private String menu;

	public String getFKMainCategory(){
		return fKMainCategory;
	}

	public String getMenu(){
		return menu;
	}
}