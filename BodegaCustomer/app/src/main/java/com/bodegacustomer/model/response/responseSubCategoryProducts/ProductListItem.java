package com.bodegacustomer.model.response.responseSubCategoryProducts;

import com.bodegacustomer.model.response.responseDashboard.NewarrivalsItem;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductListItem{

	@SerializedName("Title")
	private String title;

	@SerializedName("Newarrivals")
	private List<NewarrivalsItem> newarrivals;

	public String getTitle(){
		return title;
	}

	public List<NewarrivalsItem> getNewarrivals(){
		return newarrivals;
	}
}