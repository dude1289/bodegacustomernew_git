package com.bodegacustomer.model.response.responseMainCategory;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseMainCategory{

	@SerializedName("Status")
	private String status;

	@SerializedName("lstmenu")
	private List<LstmenuItem> lstmenu;

	public String getStatus(){
		return status;
	}

	public List<LstmenuItem> getLstmenu(){
		return lstmenu;
	}
}