package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestApplyCoupon{

	@SerializedName("FK_CustomerID")
	private String fKCustomerID;

	@SerializedName("CouponCode")
	private String couponCode;

	public void setFKCustomerID(String fKCustomerID){
		this.fKCustomerID = fKCustomerID;
	}

	public void setCouponCode(String couponCode){
		this.couponCode = couponCode;
	}
}