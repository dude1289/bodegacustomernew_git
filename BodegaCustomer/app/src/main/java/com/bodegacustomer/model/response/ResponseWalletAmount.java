package com.bodegacustomer.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseWalletAmount{

	@SerializedName("Status")
	private String status;

	@SerializedName("CustomerId")
	private String customerId;

	@SerializedName("ErrorMessage")
	private String errorMessage;

	@SerializedName("Balance")
	private String balance;

	public String getStatus(){
		return status;
	}

	public String getCustomerId(){
		return customerId;
	}

	public String getErrorMessage(){
		return errorMessage;
	}

	public String getBalance(){
		return balance;
	}
}