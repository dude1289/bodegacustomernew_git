package com.bodegacustomer.model.response.responseCoupons;

import com.google.gson.annotations.SerializedName;

public class CouponListItem{

	@SerializedName("CouponCode")
	private String couponCode;

	@SerializedName("ValidTo")
	private String validTo;

	@SerializedName("ValidFrom")
	private String validFrom;

	@SerializedName("PK_DiscForCustID")
	private String pKDiscForCustID;

	@SerializedName("DiscountAmount")
	private String discountAmount;

	public String getCouponCode(){
		return couponCode;
	}

	public String getValidTo(){
		return validTo;
	}

	public String getValidFrom(){
		return validFrom;
	}

	public String getPKDiscForCustID(){
		return pKDiscForCustID;
	}

	public String getDiscountAmount(){
		return discountAmount;
	}
}