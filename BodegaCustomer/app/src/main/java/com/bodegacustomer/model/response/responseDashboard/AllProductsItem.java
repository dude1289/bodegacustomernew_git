package com.bodegacustomer.model.response.responseDashboard;

import com.google.gson.annotations.SerializedName;

public class AllProductsItem{

	@SerializedName("ProductName")
	private String productName;

	@SerializedName("Images")
	private String images;

	@SerializedName("MRP")
	private String mRP;

	@SerializedName("Pk_ProductId")
	private String pkProductId;

	@SerializedName("OfferedPrice")
	private String offeredPrice;

	@SerializedName("ColorName")
	private Object colorName;

	@SerializedName("SoldOutCss")
	private String SoldOutCss;

	public String getSoldOutCss() {
		return SoldOutCss;
	}

	public String getProductName(){
		return productName;
	}

	public String getImages(){
		return images;
	}

	public String getMRP(){
		return mRP;
	}

	public String getPkProductId(){
		return pkProductId;
	}

	public String getOfferedPrice(){
		return offeredPrice;
	}

	public Object getColorName(){
		return colorName;
	}
}