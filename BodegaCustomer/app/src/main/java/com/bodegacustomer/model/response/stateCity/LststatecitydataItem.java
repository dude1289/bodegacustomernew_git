package com.bodegacustomer.model.response.stateCity;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class LststatecitydataItem{

    @SerializedName("Title")
    private String title;

    @SerializedName("lststatecitydetails")
    private List<LststatecitydetailsItem> lststatecitydetails;

    public String getTitle(){
        return title;
    }

    public List<LststatecitydetailsItem> getLststatecitydetails(){
        return lststatecitydetails;
    }
}