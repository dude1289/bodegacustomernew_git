package com.bodegacustomer.model.response.responseBanner;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseBanner{

	@SerializedName("Status")
	private String status;

	@SerializedName("Message")
	private String message;

	@SerializedName("lstbanner")
	private List<LstbannerItem> lstbanner;

	public String getStatus(){
		return status;
	}

	public String getMessage(){
		return message;
	}

	public List<LstbannerItem> getLstbanner(){
		return lstbanner;
	}
}