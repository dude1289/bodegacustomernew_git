package com.bodegacustomer.model.response.responseDashboard;

import com.google.gson.annotations.SerializedName;

public class BannerItem{

	@SerializedName("BannerImage")
	private String bannerImage;

	public String getBannerImage(){
		return bannerImage;
	}
}