package com.bodegacustomer.model.response.responseAddresses;

import com.google.gson.annotations.SerializedName;

public class AddressItem{

	@SerializedName("HouseNo")
	private String houseNo;

	@SerializedName("Locality")
	private String locality;

	@SerializedName("LandMark")
	private String landMark;

	@SerializedName("AddressType")
	private String addressType;

	@SerializedName("PinCode")
	private String pinCode;

	@SerializedName("CustomerOtherInfoId")
	private String Pk_AddressId;

	@SerializedName("CustomerName")
	private String CustomerName;

	@SerializedName("Contact")
	private String Contact;

	public String getCustomerName() {
		return CustomerName;
	}

	public String getContact() {
		return Contact;
	}

	public String getPk_AddressId() {
		return Pk_AddressId;
	}

	public String getHouseNo(){
		return houseNo;
	}

	public String getLocality(){
		return locality;
	}

	public String getLandMark(){
		return landMark;
	}

	public String getAddressType(){
		return addressType;
	}

	public String getPinCode(){
		return pinCode;
	}
}