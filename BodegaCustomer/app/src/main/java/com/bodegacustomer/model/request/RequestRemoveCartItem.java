package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestRemoveCartItem{

	@SerializedName("CartId")
	private String cartId;

	@SerializedName("CustomerId")
	private String customerId;

	public void setCartId(String cartId){
		this.cartId = cartId;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}
}