package com.bodegacustomer.model.response.responseProductDetails;

import com.google.gson.annotations.SerializedName;

public class LstimagesItem{

	@SerializedName("ImagePath")
	private String imagePath;

	public String getImagePath(){
		return imagePath;
	}
}