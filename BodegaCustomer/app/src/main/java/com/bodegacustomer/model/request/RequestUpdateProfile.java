package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestUpdateProfile{

	@SerializedName("Email")
	private String email;

	@SerializedName("Pk_UserId")
	private String pkUserId;

	@SerializedName("Name")
	private String name;

	@SerializedName("Contact")
	private String contact;

	public void setEmail(String email){
		this.email = email;
	}

	public void setPkUserId(String pkUserId){
		this.pkUserId = pkUserId;
	}

	public void setName(String name){
		this.name = name;
	}

	public void setContact(String contact){
		this.contact = contact;
	}
}