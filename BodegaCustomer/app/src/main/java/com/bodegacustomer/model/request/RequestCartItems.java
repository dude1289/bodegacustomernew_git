package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestCartItems{

	@SerializedName("CustomerId")
	private String customerId;

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}
}