package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestAddToCart{

	@SerializedName("ProductQuantity")
	private String productQuantity;

	@SerializedName("Fk_vendorId")
	private String fkVendorId;

	@SerializedName("CustomerId")
	private String customerId;

	@SerializedName("ProductInfoCode")
	private String productInfoCode;

	@SerializedName("Type")
	private String Type;

	public void setType(String type) {
		Type = type;
	}

	public void setProductQuantity(String productQuantity){
		this.productQuantity = productQuantity;
	}

	public void setFkVendorId(String fkVendorId){
		this.fkVendorId = fkVendorId;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public void setProductInfoCode(String productInfoCode){
		this.productInfoCode = productInfoCode;
	}
}