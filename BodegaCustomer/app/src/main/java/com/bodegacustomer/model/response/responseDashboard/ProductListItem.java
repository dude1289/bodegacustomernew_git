package com.bodegacustomer.model.response.responseDashboard;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductListItem{

	@SerializedName("Banner")
	private List<BannerItem> banner;

	@SerializedName("AllProducts")
	private List<AllProductsItem> allProducts;

	@SerializedName("FeaturedProduct")
	private List<FeaturedProductItem> featuredProduct;

	@SerializedName("Title")
	private String title;

	@SerializedName("OffersList")
	private List<OffersListItem> offersList;

	@SerializedName("Newarrivals")
	private List<NewarrivalsItem> newarrivals;

	public List<BannerItem> getBanner(){
		return banner;
	}

	public List<AllProductsItem> getAllProducts(){
		return allProducts;
	}

	public List<FeaturedProductItem> getFeaturedProduct(){
		return featuredProduct;
	}

	public String getTitle(){
		return title;
	}

	public List<OffersListItem> getOffersList(){
		return offersList;
	}

	public List<NewarrivalsItem> getNewarrivals(){
		return newarrivals;
	}
}