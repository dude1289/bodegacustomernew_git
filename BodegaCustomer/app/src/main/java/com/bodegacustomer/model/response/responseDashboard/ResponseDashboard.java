package com.bodegacustomer.model.response.responseDashboard;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseDashboard{

	@SerializedName("Status")
	private String status;

	@SerializedName("ProductList")
	private List<ProductListItem> productList;

	public String getStatus(){
		return status;
	}

	public List<ProductListItem> getProductList(){
		return productList;
	}
}