package com.bodegacustomer.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseLogin {

    @SerializedName("Status")
    private String status;

    @SerializedName("LoginId")
    private String loginId;

    @SerializedName("Fk_CustId")
    private String fkCustId;

    @SerializedName("Fk_AssociateId")
    private String fkAssociateId;

    @SerializedName("CustomerName")
    private String customerName;

    @SerializedName("ErrorMessage")
    private String errorMessage;

    @SerializedName("Password")
    private String password;

    @SerializedName("Contact")
    private String contact;

    @SerializedName("Email")
    private String Email;

    public String getEmail() {
        return Email;
    }

    public String getStatus() {
        return status;
    }

    public String getLoginId() {
        return loginId;
    }

    public String getFkCustId() {
        return fkCustId;
    }

    public String getFkAssociateId() {
        return fkAssociateId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getPassword() {
        return password;
    }

    public String getContact() {
        return contact;
    }
}