package com.bodegacustomer.model.response.responseBanner;

import com.google.gson.annotations.SerializedName;

public class LstbannerItem{

	@SerializedName("BannerImage")
	private String bannerImage;

	public String getBannerImage(){
		return bannerImage;
	}
}