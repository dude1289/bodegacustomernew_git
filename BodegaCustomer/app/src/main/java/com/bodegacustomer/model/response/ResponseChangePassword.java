package com.bodegacustomer.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseChangePassword{

	@SerializedName("NewPassword")
	private String newPassword;

	@SerializedName("Status")
	private String status;

	@SerializedName("CustomerId")
	private String customerId;

	@SerializedName("OldPassword")
	private String oldPassword;

	@SerializedName("ErrorMessage")
	private String errorMessage;

	public String getNewPassword(){
		return newPassword;
	}

	public String getStatus(){
		return status;
	}

	public String getCustomerId(){
		return customerId;
	}

	public String getOldPassword(){
		return oldPassword;
	}

	public String getErrorMessage(){
		return errorMessage;
	}
}