package com.bodegacustomer.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseSignup{

	@SerializedName("Status")
	private String status;

	@SerializedName("CustomerName")
	private String customerName;

	@SerializedName("ErrorMessage")
	private String errorMessage;

	@SerializedName("Contact")
	private String contact;

	@SerializedName("Password")
	private String password;

	public String getStatus(){
		return status;
	}

	public String getCustomerName(){
		return customerName;
	}

	public String getErrorMessage(){
		return errorMessage;
	}

	public String getContact(){
		return contact;
	}

	public String getPassword(){
		return password;
	}
}