package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestSignup{

	@SerializedName("Email")
	private String email;

	@SerializedName("SponsorCode")
	private String sponsorCode;

	@SerializedName("CustomerName")
	private String customerName;

	@SerializedName("Contact")
	private String contact;

	public void setEmail(String email){
		this.email = email;
	}

	public void setSponsorCode(String sponsorCode){
		this.sponsorCode = sponsorCode;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public void setContact(String contact){
		this.contact = contact;
	}
}