package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestSaveAddress{

	@SerializedName("HouseNo")
	private String houseNo;

	@SerializedName("Locality")
	private String locality;

	@SerializedName("LandMark")
	private String landMark;

	@SerializedName("CustomerId")
	private String customerId;

	@SerializedName("AddressType")
	private String addressType;

	@SerializedName("PinCode")
	private String pinCode;

	@SerializedName("CustomerName")
	private String CustomerName;

	@SerializedName("Contact")
	private String Contact;

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public void setContact(String contact) {
		Contact = contact;
	}

	public void setHouseNo(String houseNo){
		this.houseNo = houseNo;
	}

	public void setLocality(String locality){
		this.locality = locality;
	}

	public void setLandMark(String landMark){
		this.landMark = landMark;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public void setAddressType(String addressType){
		this.addressType = addressType;
	}

	public void setPinCode(String pinCode){
		this.pinCode = pinCode;
	}
}