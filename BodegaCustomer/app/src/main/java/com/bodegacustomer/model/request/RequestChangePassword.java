package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestChangePassword{

	@SerializedName("NewPassword")
	private String newPassword;

	@SerializedName("CustomerId")
	private String customerId;

	@SerializedName("OldPassword")
	private String oldPassword;

	public void setNewPassword(String newPassword){
		this.newPassword = newPassword;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public void setOldPassword(String oldPassword){
		this.oldPassword = oldPassword;
	}
}