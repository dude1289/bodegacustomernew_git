package com.bodegacustomer.model.response.responseMainCategory;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LstmenuItem{

	@SerializedName("Title")
	private String title;

	@SerializedName("MainMenu")
	private List<MainMenuItem> mainMenu;

	public String getTitle(){
		return title;
	}

	public List<MainMenuItem> getMainMenu(){
		return mainMenu;
	}
}