package com.bodegacustomer.model.response.responseDashboard;

import com.google.gson.annotations.SerializedName;

public class OffersListItem{

	@SerializedName("PK_OfferID")
	private String pKOfferID;

	@SerializedName("OfferImage")
	private String offerImage;

	public String getPKOfferID(){
		return pKOfferID;
	}

	public String getOfferImage(){
		return offerImage;
	}
}