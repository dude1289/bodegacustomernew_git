package com.bodegacustomer.model.response.stateCity;

import com.google.gson.annotations.SerializedName;

public class LststatecitydetailsItem{

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("Pk_Id")
    private String pkId;

    @SerializedName("State")
    private String state;

    @SerializedName("City")
    private String city;

    public String getPincode(){
        return pincode;
    }

    public String getPkId(){
        return pkId;
    }

    public String getState(){
        return state;
    }

    public String getCity(){
        return city;
    }
}