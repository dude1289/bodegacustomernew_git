package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestProductDetails{

	@SerializedName("MaterialID")
	private String materialID;

	@SerializedName("LastSelected")
	private String lastSelected;

	@SerializedName("FlavorID")
	private String flavorID;

	@SerializedName("RamID")
	private String ramID;

	@SerializedName("ProductID")
	private String productID;

	@SerializedName("StorageID")
	private String storageID;

	@SerializedName("SizeID")
	private String sizeID;

	@SerializedName("ColorID")
	private String colorID;

	public void setMaterialID(String materialID){
		this.materialID = materialID;
	}

	public void setLastSelected(String lastSelected){
		this.lastSelected = lastSelected;
	}

	public void setFlavorID(String flavorID){
		this.flavorID = flavorID;
	}

	public void setRamID(String ramID){
		this.ramID = ramID;
	}

	public void setProductID(String productID){
		this.productID = productID;
	}

	public void setStorageID(String storageID){
		this.storageID = storageID;
	}

	public void setSizeID(String sizeID){
		this.sizeID = sizeID;
	}

	public void setColorID(String colorID){
		this.colorID = colorID;
	}
}