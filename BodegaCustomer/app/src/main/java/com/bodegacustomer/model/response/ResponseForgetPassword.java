package com.bodegacustomer.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseForgetPassword{

	@SerializedName("MobileNo")
	private String mobileNo;

	@SerializedName("Status")
	private String status;

	@SerializedName("ErrorMessage")
	private String errorMessage;

	public String getMobileNo(){
		return mobileNo;
	}

	public String getStatus(){
		return status;
	}

	public String getErrorMessage(){
		return errorMessage;
	}
}