package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestSubCategoryProducts{

	@SerializedName("FK_SubCategoryId")
	private String fKSubCategoryId;

	public void setFKSubCategoryId(String fKSubCategoryId){
		this.fKSubCategoryId = fKSubCategoryId;
	}
}