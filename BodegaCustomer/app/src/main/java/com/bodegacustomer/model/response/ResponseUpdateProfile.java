package com.bodegacustomer.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseUpdateProfile{

	@SerializedName("Status")
	private String status;

	@SerializedName("Email")
	private String email;

	@SerializedName("Pk_UserId")
	private String pkUserId;

	@SerializedName("ErrorMessage")
	private String errorMessage;

	@SerializedName("Name")
	private String name;

	@SerializedName("Contact")
	private String contact;

	public String getStatus(){
		return status;
	}

	public String getEmail(){
		return email;
	}

	public String getPkUserId(){
		return pkUserId;
	}

	public String getErrorMessage(){
		return errorMessage;
	}

	public String getName(){
		return name;
	}

	public String getContact(){
		return contact;
	}
}