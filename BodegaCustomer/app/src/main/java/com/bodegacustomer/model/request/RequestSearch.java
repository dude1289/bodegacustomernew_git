package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestSearch{

	@SerializedName("ProductName")
	private String productName;

	public void setProductName(String productName){
		this.productName = productName;
	}
}