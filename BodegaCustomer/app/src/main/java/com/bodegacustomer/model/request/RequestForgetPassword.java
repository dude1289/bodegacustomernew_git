package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestForgetPassword{

	@SerializedName("MobileNo")
	private String mobileNo;

	public void setMobileNo(String mobileNo){
		this.mobileNo = mobileNo;
	}
}