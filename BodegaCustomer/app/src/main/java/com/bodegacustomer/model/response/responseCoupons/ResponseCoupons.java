package com.bodegacustomer.model.response.responseCoupons;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseCoupons{

	@SerializedName("Status")
	private String status;

	@SerializedName("PK_UserId")
	private String pKUserId;

	@SerializedName("Coupon")
	private List<CouponItem> coupon;

	public String getStatus(){
		return status;
	}

	public String getPKUserId(){
		return pKUserId;
	}

	public List<CouponItem> getCoupon(){
		return coupon;
	}
}