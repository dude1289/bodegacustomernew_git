package com.bodegacustomer.model.response.responseSubCategort;

import com.google.gson.annotations.SerializedName;

public class SubcategorylstItem {

    @SerializedName("SubCategoryName")
    private String subCategoryName;

    @SerializedName("PK_SubCategoryID")
    private String pKSubCategoryID;

    @SerializedName("ImagePath")
    private String ImagePath;

    public String getImagePath() {
        return ImagePath;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public String getPKSubCategoryID() {
        return pKSubCategoryID;
    }
}