package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestCancelOrder {

    @SerializedName("PK_OrderDetailsID")
    private String pKOrderDetailsID;

    @SerializedName("CustomerId")
    private String customerId;

    public void setPKOrderDetailsID(String pKOrderDetailsID) {
        this.pKOrderDetailsID = pKOrderDetailsID;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}