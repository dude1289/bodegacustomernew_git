package com.bodegacustomer.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestPlaceOrder{

	@SerializedName("Fk_AddressId")
	private String fkAddressId;

	@SerializedName("CustomerId")
	private String customerId;

	@SerializedName("PaymentMode")
	private String paymentMode;

	public void setFkAddressId(String fkAddressId){
		this.fkAddressId = fkAddressId;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public void setPaymentMode(String paymentMode){
		this.paymentMode = paymentMode;
	}
}