package com.bodegacustomer.model.response.stateCity;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseStateCity{

    @SerializedName("Status")
    private String status;

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("lststatecitydata")
    private List<LststatecitydataItem> lststatecitydata;

    @SerializedName("ErrorMessage")
    private String errorMessage;

    public String getStatus(){
        return status;
    }

    public String getPincode(){
        return pincode;
    }

    public List<LststatecitydataItem> getLststatecitydata(){
        return lststatecitydata;
    }

    public String getErrorMessage(){
        return errorMessage;
    }
}