package com.bodegacustomer.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.adapter.BannerAdapter;
import com.bodegacustomer.adapter.DashboardFeaturedProductsAdapter;
import com.bodegacustomer.adapter.DashboardNewArrivals;
import com.bodegacustomer.adapter.DashboardProductAdapter;
import com.bodegacustomer.adapter.MainCategoryAdapter;
import com.bodegacustomer.adapter.MainCategorySubAdapter;
import com.bodegacustomer.adapter.OffersAdapter;
import com.bodegacustomer.constants.BaseFragment;
import com.bodegacustomer.model.response.responseDashboard.ProductListItem;
import com.bodegacustomer.model.response.responseDashboard.ResponseDashboard;
import com.bodegacustomer.model.response.responseSubCategort.ResponseSubCategory;
import com.bodegacustomer.model.response.responseSubCategort.SubcategorylstItem;
import com.bodegacustomer.retrofit.SubCategoryList;
import com.bodegacustomer.retrofit.SubCategoryListener;
import com.bodegacustomer.utils.GradientTextView;
import com.bodegacustomer.utils.LoggerUtil;
import com.example.moeidbannerlibrary.banner.BannerLayout;
import com.google.gson.JsonObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dashboard extends BaseFragment implements SubCategoryList, SubCategoryListener {
    Unbinder unbinder;
    @BindView(R.id.rv_banner)
    BannerLayout rvBanner;
    @BindView(R.id.rv_offers)
    RecyclerView rvOffers;
    @BindView(R.id.rv_products)
    RecyclerView rvProducts;
    @BindView(R.id.rv_featured_products)
    RecyclerView rvFeaturedProducts;
    @BindView(R.id.rv_new_arrivals)
    RecyclerView rvNewArrivals;
    @BindView(R.id.rv_main_category)
    RecyclerView rvMainCategory;
    String subCategoryId = "", MainCategoryId = "";
    @BindView(R.id.rv_sub_category)
    RecyclerView rvSubCategory;
    @BindView(R.id.textView)
    GradientTextView tvOffers;
    @BindView(R.id.textView2)
    GradientTextView tvProducts;
    @BindView(R.id.textView3)
    GradientTextView tvFeatured;
    @BindView(R.id.textView4)
    GradientTextView tvNewArrivals;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreatedStuff(View view, @Nullable Bundle savedInstanceState) {

        rvOffers.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvFeaturedProducts.setLayoutManager(new GridLayoutManager(context, 2));
        rvProducts.setLayoutManager(new GridLayoutManager(context, 2));
        rvNewArrivals.setLayoutManager(new GridLayoutManager(context, 2));
        rvMainCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        getDashboardData();
        getmenuSubMenu();
        rvSubCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

    }


    void getDashboardData() {
        JsonObject object = new JsonObject();
        object.addProperty("FK_SubCategoryID", subCategoryId);
        object.addProperty("FK_CategoryID", MainCategoryId);

        LoggerUtil.logItem(object);

        showLoading();
        Call<ResponseDashboard> call = apiServices.GetDashboardData(object);
        call.enqueue(new Callback<ResponseDashboard>() {
            @Override
            public void onResponse(Call<ResponseDashboard> call, Response<ResponseDashboard> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    if (response.body().getProductList() != null) {
                        tvFeatured.setVisibility(View.GONE);
                        rvFeaturedProducts.setVisibility(View.GONE);

                        tvOffers.setVisibility(View.GONE);
                        rvOffers.setVisibility(View.GONE);

                        tvProducts.setVisibility(View.GONE);
                        rvProducts.setVisibility(View.GONE);

                        tvNewArrivals.setVisibility(View.GONE);
                        rvNewArrivals.setVisibility(View.GONE);
                        
                        setDashboardData(response.body().getProductList());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseDashboard> call, Throwable t) {
                hideLoading();
            }
        });
    }

    void setDashboardData(List<ProductListItem> productList) {
        for (int i = 0; i < productList.size(); i++) {
            switch (productList.get(i).getTitle()) {
                case "All Products":
                    DashboardProductAdapter adapter = new DashboardProductAdapter(context, productList.get(i).getAllProducts());
                    rvProducts.setAdapter(adapter);
                    tvProducts.setVisibility(View.VISIBLE);
                    rvProducts.setVisibility(View.VISIBLE);
                    break;
                case "Banner":
                    BannerAdapter bannerAdapter = new BannerAdapter(context, productList.get(i).getBanner());
                    rvBanner.setAdapter(bannerAdapter);
                    break;
                case "Featured Products":
                    DashboardFeaturedProductsAdapter featuredProductsAdapter = new DashboardFeaturedProductsAdapter(context, productList.get(i).getFeaturedProduct());
                    rvFeaturedProducts.setAdapter(featuredProductsAdapter);
                    tvFeatured.setVisibility(View.VISIBLE);
                    rvFeaturedProducts.setVisibility(View.VISIBLE);
                    break;
                case "New Arrivals":
                    DashboardNewArrivals dashboardNewArrivals = new DashboardNewArrivals(context, productList.get(i).getNewarrivals());
                    rvNewArrivals.setAdapter(dashboardNewArrivals);
                    tvNewArrivals.setVisibility(View.VISIBLE);
                    rvNewArrivals.setVisibility(View.VISIBLE);
                    break;
                case "Offers":
                    OffersAdapter offersAdapter = new OffersAdapter(context, productList.get(i).getOffersList());
                    rvOffers.setAdapter(offersAdapter);
                    tvOffers.setVisibility(View.VISIBLE);
                    rvOffers.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    private void getmenuSubMenu() {
        Call<ResponseSubCategory> call = apiServices.getMenuSubMenu();
        call.enqueue(new Callback<ResponseSubCategory>() {
            @Override
            public void onResponse(Call<ResponseSubCategory> call, Response<ResponseSubCategory> response) {
                MainCategoryAdapter adapter = new MainCategoryAdapter(context, response.body().getCategoryList().get(0).getCategoryDetails(), Dashboard.this, Dashboard.this);
                rvMainCategory.setAdapter(adapter);
                setSubCategoryData(response.body().getCategoryList().get(0).getCategoryDetails().get(0).getSubcategorylst());
            }

            @Override
            public void onFailure(Call<ResponseSubCategory> call, Throwable t) {

            }
        });
    }

    void setSubCategoryData(List<SubcategorylstItem> subcategorylst) {
        MainCategorySubAdapter adapterSubCategory1 = new MainCategorySubAdapter(context, subcategorylst, Dashboard.this);
        rvSubCategory.setAdapter(adapterSubCategory1);
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void getSubCategoryList(List<SubcategorylstItem> subcategorylst) {
        setSubCategoryData(subcategorylst);
    }

    @Override
    public void getSubCategoryId(String categoryId, boolean b) {
        subCategoryId = "";
        MainCategoryId = "";
        if (b)
            MainCategoryId = categoryId;
        else
            subCategoryId = categoryId;

        getDashboardData();
    }
}
