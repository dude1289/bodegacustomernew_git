package com.bodegacustomer.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.activity.ContainerActivity;
import com.bodegacustomer.adapter.AdapterCategory;
import com.bodegacustomer.adapter.DashboardNewArrivals;
import com.bodegacustomer.constants.BaseFragment;
import com.bodegacustomer.model.request.RequestSubCategoryProducts;
import com.bodegacustomer.model.response.responseSubCategort.ResponseSubCategory;
import com.bodegacustomer.model.response.responseSubCategoryProducts.ResponseSubCategoryProducts;
import com.bodegacustomer.retrofit.SubCategoryListener;
import com.bodegacustomer.utils.LoggerUtil;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductCategory extends BaseFragment implements SubCategoryListener {

    @BindView(R.id.rv_category_list)
    RecyclerView rvCategoryList;
    private Unbinder unbinder;
    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    @BindView(R.id.tv_product_category)
    TextView tvProductCategory;
    private String CategoryId = "", CategoryName = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_category, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreatedStuff(View view, @Nullable Bundle savedInstanceState) {
        rvCategories.setLayoutManager(new LinearLayoutManager(context));
        rvCategoryList.setLayoutManager(new GridLayoutManager(context, 2));

        Bundle bundle = this.getArguments();
        CategoryId = bundle.getString("CategoryId");
        CategoryName = bundle.getString("CategoryName");
        ((ContainerActivity) getActivity()).setuptoolbar(CategoryName);

        getMenu();
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    private void getMenu() {
        showLoading();
        JsonObject object = new JsonObject();
        object.addProperty("FK_MainCategory", CategoryId);
        LoggerUtil.logItem(object);

        Call<ResponseSubCategory> call = apiServices.GetMenu(object);
        call.enqueue(new Callback<ResponseSubCategory>() {
            @Override
            public void onResponse(Call<ResponseSubCategory> call, Response<ResponseSubCategory> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    try {
                        if (response.body().getCategoryList().get(0).getCategoryDetails() != null) {
                            AdapterCategory bannerAdapter = new AdapterCategory(context, response.body().getCategoryList().get(0).getCategoryDetails(), ProductCategory.this);
                            rvCategories.setAdapter(bannerAdapter);
                            if (response.body().getProductList().get(0).getNewarrivals() != null && response.body().getProductList().get(0).getNewarrivals().size() > 0) {
                                DashboardNewArrivals dashboardNewArrivals = new DashboardNewArrivals(context, response.body().getProductList().get(0).getNewarrivals());
                                rvCategoryList.setAdapter(dashboardNewArrivals);
                            }
                        } else {
                            showMessage("No Category Found");
                        }
                    } catch (Exception e) {
                        showMessage("No Category Found");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseSubCategory> call, Throwable t) {
                hideLoading();
            }
        });
    }

    private void getSubategoryProducts(String productId) {
        RequestSubCategoryProducts products = new RequestSubCategoryProducts();
        products.setFKSubCategoryId(productId);
        showLoading();
        LoggerUtil.logItem(products);

        Call<ResponseSubCategoryProducts> call = apiServices.getProductByMenu(products);
        call.enqueue(new Callback<ResponseSubCategoryProducts>() {
            @Override
            public void onResponse(Call<ResponseSubCategoryProducts> call, Response<ResponseSubCategoryProducts> response) {
                hideLoading();
                LoggerUtil.logItem(response.body());
                try {
                    if (response.body().getStatus().equalsIgnoreCase("0")) {
                        DashboardNewArrivals dashboardNewArrivals = new DashboardNewArrivals(context, response.body().getProductList().get(0).getNewarrivals());
                        rvCategoryList.setAdapter(dashboardNewArrivals);
                    }
                } catch (Exception e) {
                    showMessage("No Products Found");
                }
            }

            @Override
            public void onFailure(Call<ResponseSubCategoryProducts> call, Throwable t) {

            }
        });
    }

    @Override
    public void getSubCategoryId(String categoryId, boolean b) {
        getSubategoryProducts(categoryId);
    }
}
