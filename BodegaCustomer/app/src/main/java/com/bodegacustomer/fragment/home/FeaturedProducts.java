package com.bodegacustomer.fragment.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.adapter.DashboardFeaturedProductsAdapter;
import com.bodegacustomer.adapter.OffersAdapter;
import com.bodegacustomer.constants.BaseFragment;
import com.bodegacustomer.model.response.responseDashboard.ProductListItem;
import com.bodegacustomer.utils.GradientTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FeaturedProducts extends BaseFragment {

    private Unbinder unbinder;
    @BindView(R.id.rv_offers)
    RecyclerView rvOffers;
    @BindView(R.id.textView3)
    GradientTextView textView3;
    @BindView(R.id.rv_featured_products)
    RecyclerView rvFeaturedProducts;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_featured_products, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreatedStuff(View view, @Nullable Bundle savedInstanceState) {
        textView3.setVisibility(View.GONE);
        rvOffers.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvFeaturedProducts.setLayoutManager(new GridLayoutManager(context, 2));
        setDashboardData(Home.productList);
    }

    void setDashboardData(List<ProductListItem> productList) {
        for (int i = 0; i < productList.size(); i++) {
            switch (productList.get(i).getTitle()) {
                case "Featured Products":
                    DashboardFeaturedProductsAdapter featuredProductsAdapter = new DashboardFeaturedProductsAdapter(context, productList.get(i).getFeaturedProduct());
                    rvFeaturedProducts.setAdapter(featuredProductsAdapter);
                    break;
                case "Offers":
                    OffersAdapter offersAdapter = new OffersAdapter(context, productList.get(i).getOffersList());
                    rvOffers.setAdapter(offersAdapter);
                    break;
            }

        }
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
