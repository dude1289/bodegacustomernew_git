package com.bodegacustomer.fragment.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.adapter.DashboardProductAdapter;
import com.bodegacustomer.constants.BaseFragment;
import com.bodegacustomer.model.response.responseDashboard.ProductListItem;
import com.bodegacustomer.utils.GradientTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Products extends BaseFragment {
    Unbinder unbinder;
    @BindView(R.id.textView2)
    GradientTextView textView2;
    @BindView(R.id.rv_products)
    RecyclerView rvProducts;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_products, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreatedStuff(View view, @Nullable Bundle savedInstanceState) {
        textView2.setVisibility(View.GONE);
        rvProducts.setLayoutManager(new GridLayoutManager(context, 2));
        setDashboardData(Home.productList);
    }

    void setDashboardData(List<ProductListItem> productList) {
        for (int i = 0; i < productList.size(); i++) {
            if ("All Products".equals(productList.get(i).getTitle())) {
                DashboardProductAdapter adapter = new DashboardProductAdapter(context, productList.get(i).getAllProducts());
                rvProducts.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
