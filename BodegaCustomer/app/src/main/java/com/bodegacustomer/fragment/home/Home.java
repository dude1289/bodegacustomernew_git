package com.bodegacustomer.fragment.home;

import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.activity.ActivitySearch;
import com.bodegacustomer.constants.BaseFragment;
import com.bodegacustomer.model.response.responseDashboard.ProductListItem;
import com.bodegacustomer.model.response.responseDashboard.ResponseDashboard;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends BaseFragment {

    @BindView(R.id.rv_main_category)
    RecyclerView rvMainCategory;
    private Unbinder unbinder;
    @BindView(R.id.home_frame)
    FrameLayout homeFrame;
    @BindView(R.id.tv_short_films)
    TextView tvShortFilms;
    @BindView(R.id.tv_web_series)
    TextView tvWebSeries;
    @BindView(R.id.tv_movies)
    TextView tvMovies;

    public static List<ProductListItem> productList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreatedStuff(View view, @Nullable Bundle savedInstanceState) {

        changeTextColor(tvMovies, tvShortFilms, tvWebSeries);
        getDashboardData();
        rvMainCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
//        MainCategoryAdapter adapter = new MainCategoryAdapter(context);
//        rvMainCategory.setAdapter(adapter);
    }

    void getDashboardData() {
        JsonObject object = new JsonObject();
        object.addProperty("FK_SubCategoryID", "");

        showLoading();
        Call<ResponseDashboard> call = apiServices.GetDashboardData(object);
        call.enqueue(new Callback<ResponseDashboard>() {
            @Override
            public void onResponse(Call<ResponseDashboard> call, Response<ResponseDashboard> response) {
                hideLoading();
                if (response.body().getStatus().equalsIgnoreCase("0")) {
                    if (response.body().getProductList() != null) {
                        productList = (response.body().getProductList());
                        ReplaceFragmentWithAnimation(new NewArrivals(), "");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseDashboard> call, Throwable t) {
                hideLoading();
            }
        });
    }

    @OnClick({R.id.tv_short_films, R.id.tv_web_series, R.id.tv_movies, R.id.img_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_short_films:
                if (!(currentFragment instanceof Products)) {
                    changeTextColor(tvShortFilms, tvWebSeries, tvMovies);
                    ReplaceFragmentWithAnimation(new Products(), "simple");
                }
                break;
            case R.id.tv_web_series:
                if (!(currentFragment instanceof FeaturedProducts)) {
                    changeTextColor(tvWebSeries, tvMovies, tvShortFilms);
                    if (!(currentFragment instanceof NewArrivals))
                        ReplaceFragmentWithAnimation(new FeaturedProducts(), "reverse");
                    if (!(currentFragment instanceof Products))
                        ReplaceFragmentWithAnimation(new FeaturedProducts(), "simple");
                }
                break;
            case R.id.tv_movies:
                if (!(currentFragment instanceof NewArrivals)) {
                    changeTextColor(tvMovies, tvShortFilms, tvWebSeries);
                    ReplaceFragmentWithAnimation(new NewArrivals(), "reverse");
                }
                break;
            case R.id.img_search:
                goToActivity(ActivitySearch.class, null);
                break;
        }
    }

    private Fragment currentFragment;

    private void ReplaceFragmentWithAnimation(Fragment setFragment, String animate) {
        new Handler().postDelayed(() -> {
            currentFragment = setFragment;
            FragmentManager fragmentManager = getFragmentManager();
            assert fragmentManager != null;
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (animate.equals("simple"))
                transaction.setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out, R.animator.card_flip_left_in, R.animator.card_flip_left_out);
            else if (animate.equals("reverse"))
                transaction.setCustomAnimations(R.animator.card_flip_left_in, R.animator.card_flip_left_out, R.animator.card_flip_right_in, R.animator.card_flip_right_out);
            transaction.replace(R.id.home_frame, setFragment);
            transaction.commit();
        }, 200);
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    private void changeTextColor(TextView toGradient, TextView remove1, TextView remove2) {
        Shader textShader = new LinearGradient(0, 0, 0, 14,
                ContextCompat.getColor(context, R.color.start_color),
                ContextCompat.getColor(context, R.color.end_color),
                Shader.TileMode.CLAMP);//Assumes bottom and top are colors defined above
        toGradient.setBackground(getResources().getDrawable(R.drawable.gradient_border));
        toGradient.getPaint().setShader(textShader);
        remove1.setBackground(null);
        remove2.setBackground(null);
        remove1.getPaint().setShader(null);
        remove2.getPaint().setShader(null);
        remove1.setTextColor(getResources().getColor(R.color.colorPrimary));
        remove2.setTextColor(getResources().getColor(R.color.colorPrimary));
    }
}
