package com.bodegacustomer.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bodegacustomer.R;
import com.bodegacustomer.app.PreferencesManager;
import com.bodegacustomer.constants.BaseFragment;
import com.bodegacustomer.model.request.RequestChangePassword;
import com.bodegacustomer.model.response.ResponseChangePassword;
import com.bodegacustomer.utils.LoggerUtil;
import com.bodegacustomer.utils.NetworkUtils;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends BaseFragment {

    private Unbinder unbinder;
    @BindView(R.id.et_old_pswd)
    TextInputEditText etOldPswd;
    @BindView(R.id.et_new_pswd)
    TextInputEditText etNewPswd;
    @BindView(R.id.et_confrm_pswd)
    TextInputEditText etConfrmPswd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_password_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreatedStuff(View view, @Nullable Bundle savedInstanceState) {

    }

    @OnClick({R.id.btn_reset, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_reset:
                reset();
                break;
            case R.id.btn_submit:
                if (NetworkUtils.getConnectivityStatus(context) != 0) {
                    if (Validation()) {
                        getPasswordChange();
                    }
                } else
                    showMessage(getResources().getString(R.string.alert_internet));
                break;
        }
    }

    private void getPasswordChange() {
        RequestChangePassword requestChangePswd = new RequestChangePassword();
        requestChangePswd.setCustomerId(PreferencesManager.getInstance(context).getUserId());
        requestChangePswd.setOldPassword(etOldPswd.getText().toString().trim());
        requestChangePswd.setNewPassword(etNewPswd.getText().toString().trim());

        LoggerUtil.logItem(requestChangePswd);
        showLoading();
        Call<ResponseChangePassword> call = apiServices.getChangePassword(requestChangePswd);
        call.enqueue(new Callback<ResponseChangePassword>() {
            @Override
            public void onResponse(@NonNull Call<ResponseChangePassword> call, @NonNull Response<ResponseChangePassword> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equalsIgnoreCase("0")) {
                        showMessage("Password Changed Successfully");
                        etOldPswd.setText("");
                        etNewPswd.setText("");
                        etConfrmPswd.setText("");
                    } else {
                        showMessage(response.body().getErrorMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseChangePassword> call, @NonNull Throwable t) {
                hideLoading();
                showMessage("Something went wrong");
            }
        });
    }

    private boolean Validation() {
        if (etOldPswd.getText().toString().length() == 0) {
            etOldPswd.setError("Please enter old password");
            return false;
        } else if (etNewPswd.getText().toString().length() == 0) {
            etNewPswd.setError("Please enter new password");
            return false;
        } else if (etNewPswd.getText().toString().trim().equals(etOldPswd.getText().toString().trim())) {
            etNewPswd.setError("New Password could not be same as old password");
            return false;
        } else if (!etNewPswd.getText().toString().equals(etConfrmPswd.getText().toString())) {
            etConfrmPswd.setError("Password not matched");
            etNewPswd.setError("Password not matched");
            return false;
        }
        return true;
    }

    private void reset() {
        etConfrmPswd.setText("");
        etNewPswd.setText("");
        etOldPswd.setText("");
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
