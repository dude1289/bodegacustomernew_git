package com.bodegacustomer.fragment.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bodegacustomer.R;
import com.bodegacustomer.adapter.BannerAdapter;
import com.bodegacustomer.adapter.DashboardNewArrivals;
import com.bodegacustomer.constants.BaseFragment;
import com.bodegacustomer.model.response.responseDashboard.ProductListItem;
import com.bodegacustomer.utils.GradientTextView;
import com.example.moeidbannerlibrary.banner.BannerLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NewArrivals extends BaseFragment {

    private Unbinder unbinder;
    @BindView(R.id.rv_banner)
    BannerLayout rvBanner;
    @BindView(R.id.textView4)
    GradientTextView textView4;
    @BindView(R.id.rv_new_arrivals)
    RecyclerView rvNewArrivals;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_new_arrivals, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreatedStuff(View view, @Nullable Bundle savedInstanceState) {
        textView4.setVisibility(View.GONE);
        rvNewArrivals.setLayoutManager(new GridLayoutManager(context, 2));
        setDashboardData(Home.productList);

    }

    void setDashboardData(List<ProductListItem> productList) {
        for (int i = 0; i < productList.size(); i++) {
            switch (productList.get(i).getTitle()) {
                case "Banner":
                    BannerAdapter bannerAdapter = new BannerAdapter(context, productList.get(i).getBanner());
                    rvBanner.setAdapter(bannerAdapter);
                    break;

                case "New Arrivals":
                    DashboardNewArrivals dashboardNewArrivals = new DashboardNewArrivals(context, productList.get(i).getNewarrivals());
                    rvNewArrivals.setAdapter(dashboardNewArrivals);
                    break;

            }

        }
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
